\chapter{Bedeutung der Gruppentheorie in einfachen Beispielen}

\section{Ein einfaches Beispiel (Drehung um eine Achse)}

Wir betrachten einen dreidimensionalen euklidischen Vektorraum,
gekennzeichnet mittels $V^3$ mit einer Orthonormalbasis, gegeben durch
$\{ \hat{\bm e}_1, \hat{\bm e}_2, \hat{\bm e}_3 \}$.  In diesem Raum
betrachten wir nun eine Drehung um eine Achse $\hat{\bm n}$.

\begin{figure}[tb]
  \centering
  \def\r(#1,#2,#3){%
    {#1*sin(deg(#2))*cos(deg(#3))},%
    {#1*sin(deg(#2))*sin(deg(#3))},%
    {#1*cos(deg(#2))}%
  }
  \begin{tikzpicture}[scale=2,x={(1cm,0)},y={(30:1cm)},z={(0,1cm)}]
    \draw[->] (0,0,0) -- (1,0,0) node[right] {$\hat{\bm e}_1$};
    \draw[->] (0,0,0) -- (0,1,0) node[above right] {$\hat{\bm e}_2$};
    \draw[->] (0,0,0) -- (0,0,1) node[above] {$\hat{\bm e}_3$};
    \draw[MidnightBlue,->] (0,0,0) -- (\r(1,100,0)) node[left] {$\hat{\bm n}$};
  \end{tikzpicture}
  \caption{Der dreidimensionale euklidische Vektorraum wird
    aufgespannt durch drei orthogonale Basisvektoren, die auch als
    $x$-, $y$- und $z$-Achse bezeichnet werden.  Eine Drehachse wird
    durch eine beliebige Superposition dieser Basisvektoren
    beschrieben.}
  \label{fig:1-1}
\end{figure}

Diese Drehungen bilden eine \acct{Gruppe}
\begin{equation}
  \label{eq:1.1}
  \mathrm{SO(2)} = \{ g(\varphi), 0 \leq \varphi < 2\pi \}
\end{equation}
wobei $g(\varphi)$ erst einmal abstrakte Gruppenelemente sind, deren
Wirkung auf die Basisvektoren lautet
\begin{equation}
  \label{eq:1.2}
  \hat{\bm e}_i' = g(\varphi) \hat{\bm e}_i
  = \sum_{j=1}^3 \hat{\bm e}_j D_{ji}(g(\varphi)) \; .
\end{equation}
Die transformierten Basisvektoren wurde hier in der ursprünglichen
Basis entwickelt.

Betrachten wir nun die Wirkung auf einen Vektor $\bm v \in V^3$
\begin{subequations}
  \begin{align}
    \notag
    \bm v &= \sum_{i=1}^3 v_i \hat{\bm e}_i \\
    \notag
    \bm v' &= g(\varphi) \bm v = \sum_{i=1}^3 v_i g(\varphi) \hat{\bm e}_i \\
    \label{eq:1.3a}
    &\eqrel{eq:1.2}{=} \sum_{j=1}^3 \hat{\bm e}_j \sum_{i=1}^3 D_{ji}(g(\varphi)) v_i \\
    \intertext{also}
    \label{eq:1.3b}
    v_j' &= \sum_{i=1}^3 D_{ji}(g(\varphi)) v_i
  \end{align}
  oder mit $\bm v = (v_1,v_2,v_3)^\tp$ folgt
  \begin{equation}
    \label{eq:1.3c}
    \bm v' = \mat D(g(\varphi)) \bm v
  \end{equation}
\end{subequations}
mit der Matrixgruppe
\begin{equation}
  \label{eq:1.4}
  M = \{ \mat D(g(\varphi)) \mid 0 \leq \varphi < 2\pi \} \; .
\end{equation}
Diese ist \acct{isomorph} zu $\mathrm{SO(2)}$ und ist außerdem eine
konkrete \acct{Darstellung} der abstrakten Gruppe $\mathrm{SO(2)}$.
Diese hängt offensichtlich von der Basis $\{ \hat{\bm e}_i \}$ ab.
Die Frage ist, ob es besonders einfache Darstellungen gibt.

\section{Verschiedene Darstellungen}

Wählen wir
\begin{equation*}
  \hat{\bm e}_3 \parallel \hat{\bm n}
  \quad\text{und}\quad
  \hat{\bm e}_1, \hat{\bm e}_2 \bot \hat{\bm n} \; ,
\end{equation*}
dann zerfällt $V^3$ in eine direkte Summe
\begin{subequations}
  \begin{align}
    \label{eq:1.5a}
    V^3 &= V^1 \oplus V^2 \\
    \label{eq:1.5b}
    V^1 &= \braket{\hat{\bm e}_3} \\
    \label{eq:1.5c}
    V^2 &= \braket{\hat{\bm e}_1,\hat{\bm e}_2} \; .
  \end{align}
\end{subequations}
Dabei sind $V^1$ und $V^2$ \acct[invarianter Unterraum]{invariante Unterräume}.
\begin{align*}
  \bm v \in V^1 &\to g(\varphi) \bm v \in V^1 \text{ für alle } \varphi \; , \\
  \bm w \in V^2 &\to g(\varphi) \bm w \in V^2 \text{ für alle } \varphi \; ,
\end{align*}
oder
\begin{align*}
  \mathrm{SO(2)} V^1 \subset V^1 \; , \\
  \mathrm{SO(2)} V^2 \subset V^2 \; .
\end{align*}

In dieser Basis haben die Darstellungsmatrizen die Form
\begin{equation}
  \label{eq:1.6}
  \mat D =
  \begin{pmatrix}
    \cos\varphi & -\sin\varphi & 0 \\
    \sin\varphi & \cos\varphi  & 0 \\
    0           & 0            & 1 \\
  \end{pmatrix} \; .
\end{equation}
Wie man sieht haben sie Blockdiagonalform, d.h.\ $\mat D$ wurde
reduziert auf eine $1\times1$- und eine $2\times2$-Darstellung.  Für
einen reellen Vektorraum ist diese Darstellung irreduzibel, d.h.\ eine
weitere Zerlegung ist nicht möglich.

Betrachte nun Basisvektoren in einer komplexen Erweiterung
\begin{subequations}
  \begin{align}
    \label{eq:1.7a}
    \hat{\bm e}_{+1} &= \frac{1}{\sqrt{2}} (\hat{\bm e}_1 - \ii \hat{\bm e}_2) \\
    \label{eq:1.7b}
    \hat{\bm e}_{0} &= \hat{\bm e}_3 \\
    \label{eq:1.7c}
    \hat{\bm e}_{-1} &= -\frac{1}{\sqrt{2}} (\hat{\bm e}_1 + \ii \hat{\bm e}_2)
  \end{align}
\end{subequations}
Hier folgt
\begin{subequations}
  \label{eq:1.8}
  \begin{align}
    \notag
    g(\varphi) \hat{\bm e}_{+1}
    &\eqrel{eq:1.6}[eq:1.7a]{=} \frac{1}{\sqrt{2}}
    \begin{pmatrix}
      \cos\varphi & -\sin\varphi & 0 \\
      \sin\varphi & \cos\varphi  & 0 \\
      0           & 0            & 1 \\
    \end{pmatrix}
    \begin{pmatrix}
      1 \\ -\ii \\ 0 \\
    \end{pmatrix} \\
    \notag
    &= \frac{1}{\sqrt{2}}
    \begin{pmatrix}
      \cos\varphi + \ii \sin\varphi \\
      \cos\varphi - \ii \cos\varphi \\
      0 \\
    \end{pmatrix}
    = \cos\varphi + \ii \sin\varphi
    \begin{pmatrix}
      1 \\ -\ii \\ 0 \\
    \end{pmatrix} \\
    \label{eq:1.8a}
    &= \ee^{\ii\varphi} \hat{\bm e}_{+1}
  \end{align}
  oder für alle Vektoren
  \begin{equation}
    \label{eq:1.8b}
    g(\varphi) \hat{\bm e}_m = \ee^{\ii m \varphi} \hat{\bm e}_m \; .
  \end{equation}
\end{subequations}
Man sieht also, dass sich die Darstellung im Komplexen weiter
reduzieren lässt auf drei $1\times1$-Darstellungen.
\begin{equation}
  \label{eq:1.9}
  \mat D(g(\varphi)) \eqrel{eq:1.8b}{=}
  \begin{pmatrix}
    \ee^{\ii \varphi} & & \\
    & 1 & \\
    & & \ee^{-\ii \varphi} \\
  \end{pmatrix} \; .
\end{equation}

\section{Eine erste Konsequenz}

Betrachte einen axialsymmetrischen hermiteschen Operator $H$ im
komplexen $V^3$, d.h.
\begin{equation}
  \label{eq:1.10}
  H = D^\dagger(g(\varphi)) H D(g(\varphi)) \quad,\; 0 \leq \varphi < 2\pi
\end{equation}
und ein unitäres Produkt
\begin{equation*}
  \braket{v_1 | v_2} = \bm v_1^\dagger \bm v_2 = \sum_{i=1}^3 v_{1,i}^\dagger v_{2,i} \; .
\end{equation*}
Es folgt
\begin{align}
  \braket{\hat e_m|H|e_n}
  \notag
  &\eqrel{eq:1.10}{=} \braket{\hat e_m|D^\dagger H D|\hat e_n} \\
  \notag
  &= \braket{D \hat e_m|H|D \hat e_n} \\
  \label{eq:1.11}
  &= \ee^{\ii(n-m)\varphi} \braket{\hat e_m|H|\hat e_n} \; .
\end{align}
Die einzige mögliche Lösung für $n\neq m$ ist
\begin{equation}
  \label{eq:1.12}
  \braket{\hat e_m|H|\hat e_n} \eqrel{eq:1.11}{=} 0 \; .
\end{equation}

Symmetrieangepasste Basen erzeugen Hamiltonmatrizen in Blochform,
d.h.\ \acct[invarianter Unterraum]{invariante Unterräume} zwischen
denen Matrixelemente verschwinden.

\section{Ein weiteres Beispiel}

Sei
\begin{equation*}
  H = - \frac{\hbar^2}{2 m} \nabla^2 + V(r)
\end{equation*}
ein $\mathrm{SO(3)}$-invarianter Operator.  Dann können die
Eigenfunktionen geschrieben werden als
\begin{equation}
  \label{eq:1.13}
  \psi_{n\ell m}(r,\vartheta,\varphi) = R_{n\ell}(r) Y_{\ell m}(\vartheta,\varphi)
\end{equation}
mit Eigenwerten $E_{n\ell}$.  Sie spannen einen
$(2\ell+1)$-dimensionalen Entartungsraum $V^{(\ell)}$ auf, welcher
Eigenraum zu $H$ ist.  Mit
\begin{equation}
  \label{eq:1.14}
  g \in \mathrm{SO(3)} \colon g \bm r = \mat R(g) \bm r
\end{equation}
bedeutet eine Drehung des Zustandes
\begin{equation*}
  g \psi_{n\ell m}(\bm r)
  = \psi_{n\ell m}(\mat R^{-1}(g) \bm r)
  = \sum_{m' = \ell}^{\ell} \psi_{n\ell m'}(\bm r) D_{m'm}^{(\ell)}
\end{equation*}
wobei $\mat D^{(\ell)}$ nun eine $(2\ell+1)$-dimensionale irreduzible
Darstellung von $\mathrm{SO(3)}$ ist.  Es handelt sich dabei um eine
symmetriebedingte Entartung.

\section{Fragen an diese Vorlesung}

Wir kann man die symmetriebedingten Enartungen verstehen? Wie kann man
diese mathematisch fassen?

Welche Informationen benötige ich, um von einer Symmetrie sprechen zu
können?

Welche Beziehungen gibt es zwischen den Gruppen?  Was folgt
physikalisch daraus?

Wie kann ich Gruppentheorie nutzen, um mir Probleme zu vereinfachen?

Welche Aussagen sind sofort möglich, wenn ich eine Symmetriegruppe
kenne?

\chapter{Mathematische Grundlagen}

\section{Gruppen}

\subsection{Axiome}

\begin{definition}
  \label{def:2.1}
  Eine Gruppe ist ein Paar $(\mathcal G,\cdot)$ bestehend aus einer
  Menge $\mathcal G$ und einer Abbildung
  \begin{align*}
    \mathcal G\times\mathcal G &\to \mathcal G \\
    (g,h) &\to g \cdot h
  \end{align*}
  mit folgenden Eigenschaften:
  \begin{enumerate}
  \item Für alle $g,h,k \in \mathcal G$ gilt $g \cdot (h \cdot k) = (g
    \cdot h) \cdot k$ (Assoziativgesetz).
  \item Es gibt ein Element $e \in \mathcal G$, sodass für alle $g \in
    \mathcal G$ gilt $e \cdot g = g$ (Linkseinselement).
  \item Zu jedem $g \in \mathcal G$ existiert ein $g' \in \mathcal G$
    mit der Eigenschaft $g' \cdot g = e$ (Linksinverses $g' =
    g^{-1}$).
  \end{enumerate}
\end{definition}

%%% Local Variables: 
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "de_DE"
%%% TeX-master: "../Gruppentheorie.tex"
%%% End:
