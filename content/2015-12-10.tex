\subsection{Anmerkungen}

$\mathrm{E}(3)$ ist die Menge aller längenerhaltenden
Isomorphismen (Isometrien) des affinen Raumes $\mathbb R^3$.

In der euklidischen Geometrie heißen zwei Teilmengen $S,S'
\subset \mathbb R^3$ \acct{kongruent}, wenn es ein $g \in
\mathrm{E}(3)$ gibt mit $S' = g S$.

Translationen, Schraubungen und Gleitspiegelungen besitzen
keinen Fixpunkt.

Es gibt einen Homomorphismus
\begin{equation}
  \label{eq:3.33}
  \begin{aligned}
    \det\colon \mathrm{E}(3) &\to \mathbb Z_2 = \{-1,1\} \\
    \{\bm R,\bm t\} &\mapsto \det\bm R
  \end{aligned}
\end{equation}
mit $\ker\det = \mathrm{E}(3)$ der eigentlichen euklidschen Gruppe
oder Gruppe der starren Bewegungen.

Es gibt in einer Erweiterung die Doppelgruppe
\begin{equation*}
  \mathrm{SU}(2) \wedge \mathrm{T}(3)
\end{equation*}
mit dem Gruppenprodukt
\begin{equation*}
  \{\bm A_1,\bm t_1 \} \{ \bm A_2,\bm t_2 \}
  = \{bm A_1 \bm A_2, \bm t_1 + \bm P(\bm A_1) \bm t_2 \} \;.
\end{equation*}
Dabei ist $\bm P(\bm A_1)$ die zu $\bm A_1$ gehörende Drehmatrix aus
$\mathrm{SO}(3)$.

\section{Diskrete Symmetrien im \texorpdfstring{$\mathbb R^3$}{R³}}

Das Ziel ist Einschränkungen von $\mathbb R^3$ auf Systeme mit
diskreten ausgezeichneten Punkten verstehen.

\begin{definition}
  \label{def:3.2}
  Eine diskrete Symmetriegruppe im $\mathbb R^3$ ist eine Untergruppe
  $\mathcal G$ von $\mathrm{E}(3)$ derart, dass für jeden Punkt $\bm x
  \in \mathbb R^3$ der Orbit $\mathcal G \bm x$ nur eine endliche Zahl
  von Punkten in jedem Ball $B_r$ vom Radius $r>0$ enthält.
\end{definition}

\begin{figure}[tb]
  \centering
  \missingfigure{}
  \caption{In einem Ball mit Radius $r>0$ liegen nur endlich viele
    Punkte.}
  \label{fig:9.1}
\end{figure}

Gibt es nur einen endlichen Bereich enthält die diskrete
Symmetriegruppe keine Translationen, besitzt keinen Fixpunkt, ist
endliche Untergruppe von $\mathrm{O}(3)$ und heißt \acct{endliche
  Punktgruppe}.

Die \acct{endliche Raumgruppe} ist eine diskrete Symmetriegruppe
unendlich ausgedehnter Bereiche.

\subsection{Die Punktgruppen}

Die Punktgruppen lassen sich aus vorherigen Elementen aufbauen.
Gestartet werden kann immer mit der einfachsten
\begin{equation}
  \label{eq:3.34}
  C_n = \braket{c_n} \;.
\end{equation}
Das Hinzufügen weiterer Elemente ist nicht beliebig möglich.

\begin{example}
  $\mathrm{SU}(2)$ verwenden um neue Drehwinkel zweier
  aufeinanderfolgender Drehunegn zu berechnen
  \begin{align*}
    \bm A(\hat n,\varphi) \bm A(\hat m,\chi)
    &\eqrel{eq:3.18}{=} \left( \mathds 1 \cos\frac{\varphi}{2} - \ii (\hat n\cdot\bm\sigma) \sin\frac{\varphi}{2} \right) \left( \mathds 1 \cos\frac{\chi}{2} - \ii (\hat m\cdot\bm\sigma) \sin\frac{\chi}{2} \right) \\
    &\eqrel{eq:3.17a}{=}
    \begin{multlined}[t]
      \mathds 1 \left( \cos\frac{\varphi}{2} \cos\frac{\chi}{2} - (\hat n\cdot\hat m) \sin\frac{\varphi}{2} \sin\frac{\chi}{2} \right) \\
      -\ii\bm\sigma \left( \hat n \sin\frac{\varphi}{2} \cos\frac{\chi}{2} + \hat m \cos\frac{\varphi}{2} \sin\frac{\chi}{2} + \hat n\times\hat m \sin\frac{\varphi}{2} \sin\frac{\chi}{2} \right)
    \end{multlined} \\
    &= \mathds 1 \cos\frac{\varrho}{2} - \ii (\hat\ell\cdot\bm\sigma) \sin\frac{\varrho}{2}
  \end{align*}
  Spezialfall: $\hat n \bot \hat m$ und $\varphi = \chi = \pi/3$
  \begin{equation*}
    \cos\frac{\varrho}{2} = \left( \cos\frac{\pi}{6} \right)^2 = \frac{3}{4}
    \quad\text{oder}\quad
    \varrho \approx \frac{2\pi}{\num{4.34681}}
    \implies \varrho \notin \mathbb Q \cdot \pi
  \end{equation*}
  nicht endlich!
\end{example}

\subsection{Eigentliche und uneigentliche Punktgruppen}

Eigentliche Punktgruppen enthalten keine Inversion und keine
Spiegelung.  Die Elemente entstammen der $\mathrm{SO}(3)$.

Inversion, Spiegelung und Drehspiegelung sowie Rotoinversion sind in
uneigentlichen Punktgruppen enthalten.  Dabei gilt der folgende Satz.

\begin{theorem}
  \label{thm:3.2}
  Sei $\mathcal G$ eine endliche Punktgruppe und $\mathcal K$ der Kern des
  Homomorphismus.
  \begin{align*}
    \det\colon \mathcal G &\to \mathbb Z_2 \\
    g &\mapsto \det g
  \end{align*}
  Dann gibt es drei Fälle:
  \begin{enumerate}
  \item\label{itm:3.2-1} $\mathcal G = \mathcal K$, d.h.\ $\mathcal G$
    ist eigentliche Punktgruppe
  \item\label{itm:3.2-2} wenn $\mathcal G$ die Inversion enthält, so
    ist $\mathcal G = \mathcal K \cup i\mathcal K$.
  \item\label{itm:3.2-3} Wenn $\mathcal G \neq \mathcal K$ und $i
    \notin \mathcal G$, dann ist $\mathcal G$ isomorph zu einer
    eigentlichen Punktgruppe $\mathcal G^+ = \mathcal K \cup \mathcal
    K^+$ mit $\mathcal K^+ = \braket{ig,g\in\mathcal
      G\setminus\mathcal K}$.
  \end{enumerate}
\end{theorem}

Aus \ref{itm:3.2-2} und \ref{itm:3.2-3} liest man zwei Möglichkeiten
ab, uneigentliche Punktgruppen zu bilden.
\begin{enumerate}
\item Bilde das direkte Produkt aus einer eigentlichen Punktgruppe mit
  $\{e,i\}$, $i = \text{Inversion}$.
\item Zerlege die eigentliche Punktgruppe in Nebenklassen bezüglich
  eines Normalteilers vom Index $2$.  $\mathcal G = \mathcal N \cup g
  \mathcal N, g \notin \mathcal N$ und forme sie um zu $\hat{\mathcal
    G} = \mathcal N \cup i g \mathcal N$.
\end{enumerate}

Beispiele für Gruppen in extra Aufstellung.  Dies sind alle 14
möglichen Typen von Punktgruppen.

\subsection{Die Doppelgruppen}

Die Doppelgruppen werden in der Quantenmechanik bei Teilchen mit
halbzahligem Spin benötigt.  Gegeben sei die Projektion
\begin{equation}
  \label{eq:3.35}
  p\colon \mathrm{SU}(2) \to \mathrm{SO}(3)
\end{equation}
sowie eine Punktgruppe $\mathcal H < \mathrm{SO}(3)$.  Die Hebung der
Gruppe $\mathcal H$ von $\mathrm{SO}(3)$ nach $\mathrm{SU}(2)$ heißt
Doppelgruppe
\begin{equation}
  \label{eq:3.36}
  \bar{\mathcal H} = p^{-1}(\mathcal H) < \mathrm{SU}(2) \;.
\end{equation}
$\bar{\mathcal H}$ ist Gruppe mit der Multiplikationsregel von
$\mathrm{SU}(2)$
\begin{align*}
  g,h\in\bar{\mathcal H} \implies gh \in \bar{\mathcal H}
  \quad&\text{da}\quad p(gh) = p(g) p(h) \in \mathcal H \\
  e \in \bar{\mathcal H} \quad&\text{da}\quad p(e) = e \in \mathcal H \\
  h \in \bar{\mathcal H} \implies h^{-1} \in \bar{\mathcal H} 
  \quad&\text{da}\quad p(h^{-1}) = p(h)^{-1} \in \mathcal H
\end{align*}
$|\bar{\mathcal H}| = 2 |\mathcal H|$.  Beachte $c(\varphi+2\pi) =
c(\varphi) \bar e$ mit $\bar e = c(2\pi) \neq e \in \mathrm{SU}(2)$
für jede Drehung.

\begin{example}
  \begin{align*}
    C_3 &= \{ e,c_3,c_3^2 \} \\
    \bar C_3 &= \{ e,c_3,c_3^2,c_3^3 = \bar e,c_3^3 = c_3 \bar e,c_3^5
    = c_3^2 \bar e \}
  \end{align*}
\end{example}

%%% Local Variables:
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "de_DE"
%%% TeX-master: "../Gruppentheorie.tex"
%%% End:
