\chapter{Darstellungen}

Wir benötigen in der Physik die Wirkung von Gruppenelementen auf
Vektorräume.  Diese wollen wir hier untersuchen.

\section{Lineare und Matrixdarstellung}

\subsection{Darstellung}

\begin{definition}
  \label{def:4.1}
  Eine lineare Darstellung $D$ einer Gruppe $\mathcal G$ ist eine
  Gruppenwirkung
  \begin{equation*}
    \mathcal G \to \Aut(V,+)
  \end{equation*}
  auf den Darstellungsraum $V$, bzw.\ ein Gruppenhomomorphismus
  \begin{align*}
    D\colon \mathcal G &\to \mathrm{GL}(V) \\
    g &\mapsto D(g) \;.
  \end{align*}
\end{definition}

Bei uns meist $V = \mathbb R^n$ oder $\mathbb C^n$
(Matrixdarstellung).

Es gelten dabei folgende Bezeichnungen:
\begin{itemize}
\item Ist $D$ ein Monomorphismus (injektiv), so heißt die Darstellung
  \acct{treu}.
\item Falls $\im D = \id$: trivial, Einheitsdarstellung,
  Einsdarstellung
\item $D$ heißt unitäre Darstellung falls
  \begin{equation*}
    \braket{D(g) \bm v|D(g) \bm w} = \braket{\bm v|\bm w}
  \end{equation*}
  für alle $g \in \mathcal G$ und $\bm v,\bm w \in V$.  Andere
  Notation: $D(g)^\dagger = D(g)^{-1}$.
\item Der Grad von $D$ bezeichnet
  \begin{equation*}
    \operatorname{gr} D = \dim V \;.
  \end{equation*}
\end{itemize}

\minisec{Beispiele}
$\mathrm{GL}(n,\mathbb R)$, $\mathrm{GL}(n,\mathbb C)$,
$\mathrm{O}(n)$ sind ihre eigenen Darstellungen.

Lösungsraum $\{ f \}$ der linearen Helmholtzgleichung
\begin{equation}
  \label{eq:4.1}
  \nabla^2 f(\bm r) + k^2 f(\bm r) = 0 \;,\quad r \in \mathbb R^3, k 0 \frac{\omega}{c}
\end{equation}
$\{ f \}$ ist ein unendlich dimensionaler Vektorraum.
\begin{gather}
  \mathcal G = \mathrm{E}(3) \;,\quad g = \{ \bm f,\bm t \} \;. \notag \\
  \label{eq:4.2}
  D(f) f(\bm r) = f(g^{-1} \bm r) = f(\bm R^{-1} \bm r - \bm R^{-1} t)
\end{gather}
$D(g) f(\bm r)$ ist ebenfalls Lösung von \eqref{eq:4.1}.

Ähnliches funktioniert für die Lösung der Schrödingergleichung.

\subsection{Matrixdarstellung}

Sei $D$ eine lineare Darstellung auf $V$ und $\{\hat e_1,\dotsc,\hat
e_n\}$ eine Basis von $V$.  Dann gilt
\begin{equation}
  \label{eq:4.3}
  D(g) \hat e_i = \sum_{j=1}^n \hat e_j D_{ji}(g) \;.
\end{equation}
Aus diesen Entwicklungskoeffizienten baut man eine Matrix $\bm D(g)$
auf.
\begin{equation}
  \label{eq:4.4}\relax
  [\bm D(g)]_{ij} = D_{ij}(g) \;.
\end{equation}
Sprechweise: $\hat e_i$ transformiert sich nach der $i$-ten Spalte von
$\bm D$. Dabei
\begin{equation*}
  \bm D(g_1g_2) = \bm D(g_1) \bm D(g_2) \;.
\end{equation*}
In einer Orthonormalbasis berechnet man
\begin{equation}
  \label{eq:4.5}
  D_{ij}(g) = \braket{\hat e_i|D(g) \hat e_j} = \hat e_i^\dagger D(g) \hat e_j\;.
\end{equation}
Bei einer Basistransformation gilt
\begin{equation*}
  \hat e_i' = \sum_j \hat e_j  S_{ji} \;,\quad
  \hat e_j  = \sum_i \hat e_i' (S^{-1})_{ji}
\end{equation*}
und
\begin{subequations}
  \begin{equation}
    \label{eq:4.6a}
    D(g) \hat e_i' = \sum_j D(g) \hat e_j  S_{ji}
    \eqrel{eq:4.3}{=} \sum_{j,k} \hat e_k D(g)_{kj} S_{ji}
    = \sum_\ell \hat e_\ell' \underbrace{\sum_{k,j} S^{-1}_{\ell k} D(g)_{kj} S_{ji}}_{D'_{\ell i}(g)}
  \end{equation}
  oder
  \begin{equation}
    \label{eq:4.6b}
    \bm D'(g) = \bm S^{-1} \bm D(g) \bm S \;.
  \end{equation}
\end{subequations}

\subsection{Äquivalenz von Darstellungen}

$\bm D'$ und $\bm D$ aus \eqref{eq:4.6b} beschreiben dieselbe Wirkung
nur in verschiedenen Koordinatensystemen.  Man nennt sie äquivalent.
Dieser Begriff lässt sich verallgemeinern.

\begin{definition}
  \label{def:4.2}
  Seien $D\colon \mathcal G \to \mathrm{GL}(V)$ und $D'\colon \mathcal
  G \to \mathrm{GL}(V')$ zwei Darstellungen von $\mathcal G$.  $D'$
  heißt zu $D$ \acct{äquivalent}, wenn ein Isomorphismus
  \begin{equation*}
    S\colon V \to V'
  \end{equation*}
  existiert mit
  \begin{equation*}
    D'(g) = S^{-1} D(g) S
  \end{equation*}
  für alle $g \in \mathcal G$, d.h.\ wenn es für jedes $g\in\mathcal
  G$ ein kommutierendes Diagramm gibt:
  \begin{equation*}
    \begin{tikzcd}
      V  \arrow[r,"D(g)"] \arrow[d,"S"] & V \arrow[d,"S"] \\
      V' \arrow[r,"D'(g)"]              & V'              \\
    \end{tikzcd}
  \end{equation*}
\end{definition}

\begin{example}
  $D$ in \eqref{eq:4.3} ist in diesem Sinne äquivalent zur Matrix $\bm
  D$, $S$ ist die Abbildung auf die Koordinaten $S\colon V \to \mathbb
  C^n$.
\end{example}

\subsection{Unitäre Repräsentation}

Äquivalente Darstellungen bilden Äquivalenzklassen (Repräsentanten).
Häufig verwendet wird die unitäre Repräsentation.

\begin{theorem}
  \label{thm:4.1}
  Sei $D$ eine lineare Darstellung einer endlichen Gruppe $\mathcal G$
  auf einem unitären Vektorraum $V$ (d.h.\ einem Vektorraum mit
  unitärem Skalarprodukt $\braket{\cdot|\cdot}$).  Dann ist $D$
  äquivalent zu einer unitären Darstellung.
\end{theorem}

Zum Beweis führt man ein Skalarprodukt
\begin{equation}
  \label{eq:4.7}
  \frac{1}{\lvert\mathcal G\rvert} \sum_{g\in\mathcal G} \braket{D(g) \bm v|D(g) \bm u}
\end{equation}
ein.  Durch $1/\lvert\mathcal G\rvert$ ist eine Beschränkung auf
endlichen Gruppen gegeben.  Kann man $1/\lvert\mathcal G\rvert$ durch
ein \acct{invariantes Maß} ersetzen, so ist eine Erweiterung auf
unendliche Gruppen möglich.

\subsection{Neue Darstellungen aus alten}

Hat man bereits eine Darstellung (oder mehrere), sind folgende auch
Darstellungen:
\begin{description}
\item[Subduktion] Sei $D\colon \mathcal G \to \mathrm{GL(V)}$ eine
  Darstellung von $\mathcal G$ und $\mathcal H<\mathcal G$: Die
  Einschränkung von $D$ auf $\mathcal H$, bezeichnet als $D/\mathcal
  H$, ist Darstellung von $\mathcal H$.

\item[Direkte Summe] Seien $U$ und $V$ Untervektorräume von $W$, $W =
  U \oplus V$, $\bm w = (\bm u,\bm v)^\tp$, sowie
  \begin{align*}
    D_1\colon \mathcal G &\to \mathrm{GL}(U) \\
    D_2\colon \mathcal G &\to \mathrm{GL}(V)
  \end{align*}
  Darstellungen.  Dann ist
  \begin{equation*}
    D = D_1 \oplus D_2\colon \mathcal G \to \mathrm{GL}(W)
  \end{equation*}
  mit 
  \begin{equation}
    \label{eq:4.8}
    (D_1\oplus D_2)(\bm u\oplus\bm v) \defineeq D_1 \bm u \oplus D_2 \bm v
  \end{equation}
  eine Darstellung auf $W$ mit Darstellungsmatrizen
  \begin{equation}
    \label{eq:4.9}
    \bm D(g) =
    \begin{pmatrix}
      \bm D_1(g) & 0 \\
      0 & \bm D_2(g) \\
    \end{pmatrix} \;.
  \end{equation}
  Diese Blockform kann bei einem Basiswechsel in $W$ verloren gehen.

\item[Direktes Produkt] Sei $V$ Vektorraum mit Basis $\{ \bm
  v_1,\dotsc,\bm v_n \}$ und $W$ Vektorraum mit Basis $\{ \bm
  w_1,\dotsc,\bm w_n \}$. Seien weiterhin
  \begin{align*}
    D_1\colon \mathcal G &\to \mathrm{GL}(V) \\
    D_2\colon \mathcal G &\to \mathrm{GL}(W)
  \end{align*}
  Darstellungen.  Dann ist
  \begin{align*}
    D = D_1 \otimes D_2 \colon \mathcal G &\to \mathrm{GL}(V \otimes W) \\
    g &\mapsto D(g) \\
    D(g)\colon V \otimes W &\to V \otimes W \\
    \bm v \otimes \bm w &\mapsto D_1(g) \bm v \otimes D_2(g) \bm w
  \end{align*}
  eine Produktdarstellung auf $V\otimes W$.
  
  Die typische Anwendung ist die Quantenmechanik mit mehreren
  Freiheitsgraden.
  \begin{equation*}
    \ket{\uparrow\uparrow} = \ket{\uparrow}_1 \otimes \ket{\uparrow}_2 \; .
  \end{equation*}
\end{description}

\section{Reduzible und irreduzible Darstellungen}

Woher weiß man, dass man bei einer optiomalen Darstellung im Sinn von
\eqref{eq:4.9} angekommen ist.

\subsection{Invariante Unterräume}

\begin{definition}
  \label{def:4.3}
  Sei $D$ eine endlichdimensionale Darstellung einer Gruppe $\mathcal
  G$ auf dem Vektorraum $V$.  Ein Untervektorraum $W$ von $V$ heißt
  \acct{invariant} unter $D$, wenn $D(g)\bm w \in W$ für alle $\bm w
  \in W$ und alle $g \in \mathcal G$ gilt.  In einem solchen Fall kann
  man eine Darstellung $D'\colon \mathcal G \to \mathrm{GL}(W)$ als
  Einschränlung $D'(g) = D(g)|_W$ für jedes $g \in \mathcal G$
  definieren.  Ist $D$ unitär, so auch $D'$.
\end{definition}

\begin{definition}
  \label{def:4.4}
  Die Darstellung ist \acct{reduzibel}, wenn es einen echten
  invarianten Unterraum gibt.  Ansonsten ist $D$ \acct{irreduzibel}.
  Eindimensionale Darstellungen sind irreduzibel.
\end{definition}

Nichttrivialer Vektorraum: $W \neq \{ 0 \}$.  Echter Unterraum: $W
\neq V$, $W \neq \{ 0 \}$.

Konsequenz: In einer geeigneten Basis $\bm v = (\bm w,\ldots)^\tp$
haben die Darstellungsmatrizen die Form
\begin{equation}
  \label{eq:4.10}
  \bm D =
  \begin{pmatrix}
    \bm D'(g) & \bm N(g) \\
    0 & \bm D''(g) \\
  \end{pmatrix} \;.
\end{equation}





%%% Local Variables:
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "deutsch"
%%% TeX-master: "../Gruppentheorie.tex"
%%% End:
