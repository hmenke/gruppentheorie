\subsection{Störungsrechnung}

Unser Ausgangspunkt ist ein Hamiltonoperator, der aus einem gestörten
und ungestörten Anteil besteht
\begin{equation*}
  H = H_0 + H_1 \;.
\end{equation*}
Dabei hat $H_0$ die Symmetrie $\mathcal G_0$ und $H_1$ die Symmetrie
$\mathcal G_1$.  Das Energieniveau $E_0$ von $H_0$ ist entartet.
Spaltet es mit der Störung auf?

\subsubsection{\texorpdfstring{$\mathcal G_1 = G_0$}{G1 = G0}}
\label{sec:G1=G0}

Es gibt drei Fälle zu unterscheiden.
\begin{enumerate}
\item Der Unterraum $V^{(E_9)}$ zum Eigenwert $E_0$ gehört zu einer
  \acct*{irreduziblen} Darstellung von $\mathcal G$.  D.h.\ $H_1$ verursacht
  nur eine Energieverschiebung.
\item\label{itm:G1=G0-2} Der Entartungsraum ist \acct*{reduzibel},
  aber jede Darstellung kommt nur ein Mal vor:
  \begin{equation*}
    V^{(E_0)} = \bigoplus_{\alpha=1}^{k} V^{(k)} \;.
  \end{equation*}
  Die Störmatrix ist diagonal.  Es liegt eine zufällige Entartung vor.
  Diese wird im allgemeinen durch die Störung $H_1$ aufgehoben.
\item\label{itm:G1=G0-3} Der Entartungsraum ist reduzibel, aber eine
  Darstellung kommt mehrfach vor.  Es müssen Untermatrizen der Form
  \eqref{eq:5.29b} diagonalisiert werden.
\end{enumerate}

\subsubsection{\texorpdfstring{$\mathcal G_1 < G_0$}{G1 < G0}}

Die Darstellung $D_0^{(\alpha)}$ ist i.a.\ für die Gruppe $\mathcal
G_1$ reduzibel, vgl.~\eqref{eq:5.19}.
\begin{equation*}
  D_0^{(\alpha)} = \bigoplus_{\beta=1}^{k} n_\beta D_1^{(\beta)} \;.
\end{equation*}
Aufspaltung in $\sum_{\beta=1}^{k} n_\beta$ Terme.  Vorgehen wie in
\ref{sec:G1=G0} \ref{itm:G1=G0-2} und \ref{itm:G1=G0-3}.

\section{Übergangsmatrixelemente und Auswahlregeln}

Ausgangspunkt ist ein Hamiltonoperator $H_0$ mit Eigenzuständen
$\ket{a}$ und $\ket{b}$.  Einschalten einer Störung, z.B.\ eine
elektromagnetische Welle zum Zeitpunkt $t=0$.
\begin{subequations}
  \begin{equation}
    \label{eq:5.31a}
    H_1(t) = \Theta(t) \bigl[ F \ee^{\ii\omega t} + F^\dagger \ee^{-\ii\omega t} \bigr]
  \end{equation}
  Die Übergangsrate berechnet sich zu
  \begin{equation}
    \label{eq:5.31b}
    \Gamma_{a\to b} = \frac{2\pi}{\hbar} \bigl[ \delta(E_b-E_a-\hbar\omega) \lvert\braket{b|F|a}\rvert^2
    + \delta(E_b-E_a+\hbar\omega) \lvert\braket{b|F|a}\rvert^2 \bigr]
  \end{equation}
  mit
  \begin{equation}
    \label{eq:5.31c}
    F \sim \bm p \cdot \bm E \quad\text{(Dipolnäherung)} \;.
  \end{equation}
\end{subequations}
Es tritt also auf $\braket{b|\bm p|a}$.  Für solche Betrachtungen sind
also Vektor- oder allgemeiner Tensoroperatoren zu berücksichtigen.
Symmetrien müssen bezüglich aller Komponenten beachtet werden.  Um es
besonders einfach zu machen sortieren wir die Komponenten so, dass
irreduzible Darstellungen der Symmetrie auftreten.

\subsection{Tensoroperatoren}

Als erstes müssen wir das Verhalten verstehen.  Betrachte die
Observable $S$ und unitäre Darstellung $D(g)$ von $\mathcal G$
\begin{align}
  \notag
  D(g)\colon V &\to V \\
  \label{eq:5.32}
  \psi &\mapsto D(g) \psi \quad\text{mit}\quad D(g^{-1}) = D(g)^{-1} = D(g)^\dagger \;.
\end{align}
$S'$ ist dann der Operator, der auf die mit $g$ transformierten
Elemente der Hilbertraums wirkt.
\begin{equation}
  \label{eq:5.33}
  \begin{tikzcd}
    V \ar[r,"D(g)"] \ar[d,"S"] & V \ar[d,"S'"] \\
    V \ar[r,"D(g)"] & V \\
  \end{tikzcd}
  \quad\text{Folglich:}\ S' = D(g) S D(g^{-1})
\end{equation}

\begin{definition}
  \label{def:5.1}
  Die Menge $\{S_i^{(\alpha)}, i = 1,\dots,d_\alpha\}$ bildet einen
  Satz von \acct{irreduziblen Tensoroperatoren}, wenn sich ihre
  Elemente nach der irreduziblen Darstellung $D^{(\alpha)}$ von
  $\mathcal G$ transformieren.
  \begin{equation}
    \label{eq:5.34}
    D(g) S_i^{(\alpha)} D(g^{-1}) = \sum_{j=1}^{d_\alpha} S_j^{(\alpha)} D_{ji}^{(\alpha)}(g) \;.
  \end{equation}
\end{definition}

\subsection{Beispiele}

\subsubsection{Ortsoperator}

Frage: Wie transformiert sich der Ortsoperator $x_i$?  Ist er
Tensoroperator?  Wir beginnen mit bekanntem Wissen: Transformation der
Basisvektoren.
\begin{equation*}
  \hat e_i' \eqrel{eq:1.2}{=} \sum_{j=1}^{3} \hat e_j D_{ji}^{(V)}(g) = D^{(V)}(g) \hat e_i
\end{equation*}
Vektor in Orthonormalbasis
\begin{equation}
  \label{eq:5.35}
  \bm r = x_1 \hat e_1 + x_2 \hat e_2 + x_3 \hat e_3
\end{equation}
Komponenten des transformierten Vektors
\begin{equation}
  \label{eq:5.36}
  D^{(V)}(g) \bm r \eqrel{eq:5.35}{=} \sum_i x_i D^{(V)}(g) \hat e_j \eqrel{eq:1.2}{=} \sum_{i,j} x_i D_{ji}^{(V)}(g) \hat e_j
\end{equation}
Damit
\begin{subequations}
  \begin{align}
    \label{eq:5.37a}
    (D^{(V)}(g) \bm r)_j = \sum_i x_i D_{ji}(g) \\
    \label{eq:5.37b}
    (D^{(V)}(g^{-1}) \bm r)_i = \sum_j D_{ji}(g) x_j
  \end{align}
\end{subequations}
Wirkung auf ein Element des Hilbertraums, skalare Funktion
\begin{equation}
  \label{eq:5.38}
  D(g) \psi(x_1,x_2,x_3) = \psi((D^{(V)}(g^{-1}) \bm r)_1,(D^{(V)}(g^{-1}) \bm r)_2,(D^{(V)}(g^{-1}) \bm r)_3) \equiv \phi(\bm r)
\end{equation}
Die Wirkung des Ortsoperators ist aus der Quantenmechanik bekannt
\begin{equation}
  \label{eq:5.39}
  \hat x_i \psi(x_1,x_2,x_3) = x_i \psi(x_1,x_2,x_3)
\end{equation}
Das folgende kommutierende Diagramm beschreibt dann die Wirkung im
transformierten Raum.
\begin{equation*}
  \begin{tikzcd}
    \psi \ar[r,"D(g)"] \ar[d,"x_i"] & D(g) \psi = \phi \ar[d,"x_i'"] \\
    x_i \psi \ar[r,"D(g)"] & x_i' \phi = D(g) x_i D(g^{-1}) \phi \\
  \end{tikzcd}
\end{equation*}
Jetzt berechnet man
\begin{multline}
  \label{eq:5.40}
  x_i' \phi(\bm r) = D(g) x_i D(g^{-1}) \phi(\bm r)
  \eqrel{eq:5.38}{=} D(g) \underbrace{x_i \phi(D^{(V)}(g) \bm r)}_{\tilde\phi(\bm r) \in V} \\
  = \tilde\phi(D^{(V)}(g^{-1}) \bm r)
  = (D^{(V)}(g^{-1}) \bm r)_i \phi(\bm r)
\end{multline}
Man liest ab
\begin{equation}
  \label{eq:5.41}
  D(g) \hat x_i D(g^{-1}) \eqrel{eq:5.40}[eq:5.39]{=} (D^{(V)}(g^{-1}) \hat{\bm r})_i
  \eqrel{eq:5.37b}{=} \sum_j D_{ji}^{(V)}(g) \hat x_j
\end{equation}
$\bm r$ transformiert sich wie die Basisvektoren $\hat e_i$, erfüllt
\eqref{eq:5.34} und ist somit irreduzibler Tensoroperator
(Vektoroperator).

\subsubsection{Impulsoperator}

\begin{equation}
  \label{eq:5.42}
  \hat p_i \psi(x_1,x_2,x_3) = \frac{\hbar}{\ii} \frac{\partial}{\partial x_i} \psi(x_1,x_2,x_3)
\end{equation}
Diagramm
\begin{equation*}
  \begin{tikzcd}
    \psi \ar[r,"D(g)"] \ar[d,"p_i"] & D(g) \psi = \phi \ar[d,"p_i'"] \\
    \frac{\hbar}{\ii} \frac{\partial}{\partial x_i} \psi \ar[r,"D(g)"] &
    \frac{\hbar}{\ii} \frac{\partial}{\partial x_i'} \phi = D(g) \frac{\hbar}{\ii} \frac{\partial}{\partial x_i} D(g^{-1}) \phi \\
  \end{tikzcd}
\end{equation*}
Dazu eine Nebenrechnung
\begin{align}
  \notag
  \frac{\partial}{\partial x_j} (D(g) \psi)(\bm r)
  &\eqrel{eq:5.38}{=} \frac{\partial}{\partial x_j} \psi(D^{(V)}(g^{-1}) \bm r) \\
  \notag
  &= \sum_i \psi_{,i} (D^{(V)}(g^{-1})\bm r) \frac{\partial(D^{(V)}(g^{-1})\bm r)_i}{\partial x_j} \\
  \label{eq:5.43}
  &\eqrel{eq:5.37b}{=} \sum_i \psi_{,i}(D^{(V)}(g^{-1})\bm r) D_{ji}^{(V)}(g)
\end{align}
Also
\begin{equation}
  \label{eq:5.44}
  (D(g) \psi)_{,j} \eqrel{eq:5.43}{=} \sum_i \psi_{,i}(D^{(V)}(g^{-1})\bm r) D_{ji}^{(V)}(g)
\end{equation}
Durch Multiplikation mit $D_{jk}^{(V)}(g)$ und Ausnutzung der
Orthogonalitätsrelationen
\begin{subequations}
  \begin{equation}
    \label{eq:5.45a}
    D(g) (\psi_{,i}) \eqrel{eq:5.44}{=} \sum_{j=1}^3 (D(g) \psi)_{,j} D_{ji}^{(V)}(g)
  \end{equation}
  oder
  \begin{equation}
    \label{eq:5.45b}
    D(g) \hat p_i D(g^{-1}) = \sum_{j=1}^3 \hat p_j \underbrace{(D(g) \psi)}_{\phi} D_{ji}^{(V)}(g) = \hat p_i'
  \end{equation}
\end{subequations}
aus dem kommutierenden Diagramm.  Der Impulsoperator transformiert
sich wie $\hat e_i$ und $\hat x_i$ und ist ein Vektoroperator.


%%% Local Variables:
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "deutsch"
%%% TeX-master: "../Gruppentheorie.tex"
%%% End:
