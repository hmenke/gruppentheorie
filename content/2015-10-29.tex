Insbesondere die Automorphismengruppe wird wichtig werden, daher ein
paar Beispiele:
\begin{itemize}
\item Sei $\mathcal M = \{1,2,\ldots,n\}$ eine Menge mit $n$ Objekten.
  Dann ist $\Aut\mathcal M$ die Gruppe aller bijektiven
  Abbildungen der Menge auf sich.  Dies ist hier die
  Permutationsgruppe $\mathcal S_n = \mathcal S(\mathcal M)$.
\item Die Struktur reduziert gegebenenfalls die Automorphismengruppe.
  Sei $\mathcal M=\mathbb R^3$ (linearer Vektorraum), dann ist
  $\Aut\mathcal M=\mathrm{GL}(3,\mathbb R)$.  Sie aber $\mathcal
  M=(\mathbb R^3,\cdot)$ (euklidischer Vektorraum), dann muss auch das
  Skalarprodukt erhalten bleiben, aus $\mathrm{GL}(3,\mathbb R)$
  bleiben nur die Drehungen und Spiegelungen übrig, es ist also
  $\Aut\mathcal M=\mathrm{O}(3) < \mathrm{GL}(3,\mathbb R^3)$.
\end{itemize}

\section{Die Symmetrie in der Gruppentheorie}

\subsection{Gruppenwirkung}

\begin{definition}
  \label{def:2.8}
  Sei $\mathcal G$ eine Gruppe.  Die \acct{Wirkung} von $\mathcal G$
  auf eine Menge $\mathcal M$ ist ein Homomorphismus.
  \begin{align*}
    h\colon \mathcal G &\to \Aut\mathcal M \\
    g &\mapsto h(g) \\
    \text{wobei}\quad h(g)\colon \mathcal M &\to \mathcal M \\
    x &\mapsto h(g) x
  \end{align*}
\end{definition}

Was sagt uns das?  Eine Gruppe wirkt auf die Menge in der Art, dass
alle möglichen Wirkungen bijektive Abbildungen der Menge auf sich
selbst sind, dass jedem Gruppenelement eine dieser Abbildungen
zugeordnet wird und dass jede dieser Abbildungen $h(g)$ einem Element
$x$ eindeutig ein neues zuordnet.

\begin{example}
  Die Menge sei $(\mathbb R^3,\cdot)$ und $\Aut(\mathbb
  R^3,\cdot)=\mathrm{O}(3)$.  Dann ist die Wirkung auf ein Element
  $\bm r \in (\mathbb R^3,\cdot)$
  \begin{align*}
    \mathrm{O}(3) &\to \mathcal M \\
    g &\mapsto \bm D(g) \\
    h(g)\colon (\mathbb R^3,\cdot) &\to (\mathbb R^3,\cdot) \\
    \bm r &\mapsto \bm D(g) \bm r
  \end{align*}
  wobei $\mathcal M$ die Matrixgruppe aus Gleichung~\eqref{eq:1.4} ist.
\end{example}

Wenn wir das als bekannt akzeptieren, können wir die Wirkung auf eine
Funktion $\psi(\bm r)$ definieren, wobei $\psi\in\mathrm(C)(\mathbb
R^3)$ eine stetige Funktion auf dem $\mathbb R^3$ ist.
\begin{align*}
  \mathrm{O}(3) &\to \Aut\mathrm{C}(\mathbb R^3) \\
  g &\mapsto h(g) \\
  h(g)\colon \mathrm{C}(\mathbb R^3) &\to \mathrm{C}(\mathbb R^3) \\
  \psi &\mapsto h(g) \psi(\bm r) \defineeq \psi(\bm D^{-1}(g)\bm r)
\end{align*}

\subsection{Symmetrie und Symmetriegruppe}

\begin{definition}
  \label{def:2.9}
  Es wirke eine Gruppe $\mathcal G$ auf eine Menge $M$.  Sei $a\in M$.
  Die Untergruppe $\mathcal H = \{h\in\mathcal G \mid ha = a\} <
  \mathcal G$ heißt Symmetriegruppe, Symmetrie oder Fixpunktegruppe
  von $a$.
\end{definition}

\begin{example}
  Sei $\Delta = \{x_1,x_2,x_3\}$ ein gleichseitiges Dreieck und eine
  Gruppe $\mathcal G=\mathrm{SO}(3)$.  Dann ist die Symmetriegruppe
  $C_3 = \{ e, c_3=R(\SI{120}{\degree}), c_3^2=R(\SI{240}{\degree}) \}
  < \mathrm{SO}(2)$.
\end{example}

\subsection{Gruppendarstellung}

\begin{definition}
  \label{def:2.10}
  Eine \acct{lineare Darstellung} der Gruppe $\mathcal G$ ist die
  Wirkung
  \begin{align*}
    \mathcal G &\to \Aut\mathbb R^n = \mathrm{GL}(n,\mathbb R)
    \text{ oder} \\
    \mathcal G &\to \Aut\mathbb C^n = \mathrm{GL}(n,\mathbb C)
    \text{ oder allgemein} \\
    \mathcal G &\to \Aut V \text{ mit dem Vektorraum $V$} \;.
  \end{align*}
  Dies wird später noch ausführlich behandelt.
\end{definition}

\section{Nebenklassen}

Häufig werden Gruppen in Äquivalenzklassen eingeteilt.  Dafür
benötigen wir Äquivalenzrelationen.

\begin{definition}
  \label{def:2.11}
  Eine Beziehung $\sim$ zwischen zwei Elementen $g,h$ einer Menge
  $\mathcal M$ (d.h.\ eine Teilmenge von $\mathcal M\times\mathcal M$
  heißt Äquivalenzrelation, wenn gilt:
  \begin{enumerate}
  \item Sie ist reflexiv: $g\sim g$
  \item Sie ist symmetrisch: wenn $g\sim h$, dann $h\sim g$
  \item Sie ist transitiv: wenn $g\sim h$ und $h\sim k$, dann $g\sim k$
  \end{enumerate}
\end{definition}

Die \acct{Äquivalenzklasse} $[g]\subset\mathcal M$ besteht aus allen
zu $g$ äquivalenten Elementen der Menge
\begin{equation}
  \label{eq:2.1}\relax
  [g] = \{ h \in \mathcal M\mid h\sim g \} \;.
\end{equation}
$g$ heißt \acct{Repräsentant} der Klasse.

Der folgende Satz zeigt, warum das so wertvoll ist.
\begin{theorem}
  \label{thm:2.2}
  Durch eine Äquivalenzrelation wird eine Menge in vollständig
  disjunkte Klassen zerlegt.
  \begin{equation}
    \label{eq:2.2}
    \mathcal M = \bigcup_{g} [g]
    \quad\text{und}\quad
    [g] \cap [h] = \emptyset \iff g \not\sim h
  \end{equation}
\end{theorem}

\begin{proof}
  Es kann keine leere Äquivalenzklasse geben, also $[g]\neq\emptyset$,
  da $g\sim g$ sein muss.  Jedes Element von $\mathcal M$ liegt also
  in irgendeiner Klasse $\implies \mathcal M = \bigcup_{g} [g]$.

  Sei $[g] \cap [h] = x\in\mathcal M$, dann gilt $g\sim x$ und $h\sim
  x$. Auf Grund der Transitivität auch $g\sim h$.  Dies für beliebige
  Elemente aus $[g]$, $\tilde g\in [g] = \tilde g\in [h]$ und
  umgekehrt, also $[g]=[h]$.  Es gilt also $[g]\cap [h] = \emptyset$
  oder $[g]=[h]$.
\end{proof}

\begin{example}
  Die ganzen Zahlen $z_1,z_2,n_1,n_2$ erfüllen $z_1 n_2 = z_2 n_1$ und
  es gilt
  \begin{equation*}
    r_1 = \frac{z_1}{n_1} \sim \frac{z_2}{n_2} = r_2 \; .
  \end{equation*}
  Rationale Zahlen bilden eine Äquivalenzklasse.
\end{example}

Uns interessieren besonders Äquivalenzrelationen für Gruppen.

\begin{theorem}
  \label{thm:2.3}
  Sei $\mathcal H$ eine Untergruppe von $\mathcal G$ und seien
  $g,h\in\mathcal G$.  Dann ist die Beziehung \acct[äquivalent
  modulo]{äquivalent modulo $\mathcal{H}$}, geschrieben
  $\underset{\bmod\mathcal H}{\sim}$ mit
  \begin{subequations}
    \begin{gather}
      \label{eq:2.3a}
      g \underset{\bmod\mathcal H}{\sim} h \iff g^{-1} h \in \mathcal H \\
      \label{eq:2.3b}
      \text{d.h.}\quad
      g \mathcal H \defineeq \{ gh \mid h \in \mathcal H \}
    \end{gather}
  \end{subequations}
  eine Äquivalenzrelation in $\mathcal G$.
\end{theorem}

\begin{proof}
  \begin{itemize}
  \item Reflexivität: $a^{-1}a\in\mathcal H$, weil $e\in\mathcal H$,
    also $a\underset{\bmod\mathcal H}{\sim}a$
  \item Symmetrie: Sei $a^{-1}b\in\mathcal H$, da $\mathcal H$
    Untergruppe muss auch $(a^{-1}\cdot b)^{-1} = b^{-1} a\in\mathcal
    H$ sein, also $a\underset{\bmod\mathcal H}{\sim}b \iff
    b\underset{\bmod\mathcal H}{\sim}a$.
  \item Transitivität: $a^{-1}b\in\mathcal H, b^{-1}c\in\mathcal H
    \implies (a^{-1}b)(b^{-1}c) = a^{-1}c\in\mathcal H$.
  \end{itemize}
\end{proof}

Dieser Satz kann verwendet werden um wichtige Begriffe einzuführen,
insbesondere die Schreibweise \eqref{eq:2.3b} hat einen eigenen Namen.

\begin{definition}
  \label{def:2.12}
  Die Äquivalenzklasse $[g] \eqrel{eq:2.3b}{=} g \mathcal H$ heißt
  \acct{Linksnebenklasse} von $g$ bezüglich $\mathcal H$.
\end{definition}

\begin{definition}
  \label{def:2.13}
  Die Menge aller Linksnebenklassen bezüglich $\mathcal H$ heißt
  \acct{Faktormenge} $\mathcal G/\mathcal H$.

  Die Abbildung $\pi\colon \mathcal G\to\mathcal G/\mathcal H$ mit
  $g\mapsto [g]=g\mathcal H$ heißt \acct{Projektion}.
\end{definition}

%%% Local Variables: 
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "de_DE"
%%% TeX-master: "../Gruppentheorie.tex"
%%% End:
