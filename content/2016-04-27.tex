\subsubsection{Hamiltonoperator}

Da der Hamiltonoperator
\begin{equation*}
  H = \frac{p^2}{2 m} + V(r)
\end{equation*}
die Symmetrie $\mathcal G$ besitzt, gilt
\begin{equation*}
  D(g) H D(g^{-1}) = H
\end{equation*}
$H$ transformiert sich nach der Einsdarstellung.

\subsection{Operatorprodukte}

Zwei Sätze von Operatoren transformieren sich nach der
Produktdarstellung
\begin{align}
  \label{eq:5.46}
  D_{lm,jn}^{(\alpha\otimes\beta)}(g) = D_{lj}^{(\alpha)}(g) D_{mn}^{(\beta)}(g) \\
  \label{eq:5.47}
  \hat x_j' \hat p_k' = D_{lj}^{(V)}(g) D_{mn}^{(V)}(g) \hat x_l \hat p_m
\end{align}
Damit lässt sich auch das Transformationsverhalten des Drehimpulses
bestimmen.
\begin{equation}
  \label{eq:5.48}
  \hat l_i = \varepsilon_{ijk} \hat x_j \hat p_k \;,\quad
  \hat l_i \varepsilon_{irs} = \hat x_r \hat p_s - \hat x_s \hat p_r \\
\end{equation}
\begin{align}
  \notag
  D(g) \hat l_i D(g^{-1}) &\eqrel{eq:5.48}[eq:5.47]{=}
  \varepsilon_{ijk} D_{lj}^{(V)}(g) D_{mn}^{(V)}(g) \hat x_l \hat p_m
  = - \varepsilon_{ijk} D_{lj}^{(V)}(g) D_{mn}^{(V)}(g) \hat x_m \hat p_l \\
  \notag
  &= \frac{1}{2} \varepsilon_{ijk} D_{lj}^{(V)}(g) D_{mn}^{(V)}(g) (\hat x_l \hat p_m - \hat x_m \hat p_l) = \frac{1}{2} \varepsilon_{slm} \varepsilon_{ijk} D_{lj}^{(V)}(g) D_{mn}^{(V)}(g) \hat l_s \\
  \label{eq:5.49}
  &= \det(D^{(V)}(g)) D_{si}^{(V)} \hat l_s
\end{align}
Transformationsverhalten axialer Vektoren.

\subsection{Wigner-Eckart-Theorem}
\index{Wigner-Eckart-Theorem}

Nun können wir eine wesentliche Aussage über das
Verschwinden/Nichtverschwinden von Übergangsmatrixelementen fassen
(Auswahlregeln).  Sei $\{ S_i^{(\alpha)}, i=1,\dots,d_\alpha \}$ ein
Satz irreduzibler Tensoroperatoren zu $D^{(\alpha)}$ und $\{
\psi_j^{(\beta)}, j=1,\dots,m \}$ ein Satz von Basisfunktionen zu
$D^{(\beta)}$.  Dann gilt für eine Transformation
\begin{equation*}
  D(g) S_i^{(\alpha)} \psi_j^{(\beta)} = D(g) S_i^{(\alpha)} D(g^{-1}) D(g) \psi_j^{(\beta)}
  \eqrel{eq:5.34}{=} \sum_{k,s} S_k^{(\alpha)} \psi_l^{(\beta)} D_{ki}^{(\alpha)}(g) D_{lj}^{(\beta)}(g)
\end{equation*}
Die Produktdarstellung ist im Allgemeinen reduzibel.  Wir entwickeln
nach irreduziblen Darstellungen $\gamma$.
\begin{equation}
  \label{eq:5.50}
  S_i^{(\alpha)} \psi_j^{(\beta)} \eqrel{eq:4.56}{=} \sum_{\gamma,s,k} \varphi_k^{(\gamma,s)}
  \braket{\gamma s k|\alpha i, \beta j}
\end{equation}
Für uns interessant sind Übergänge, vermittelt durch die
Matrixelemente.
\begin{align}
  \notag
  & \braket{\psi_m^{(s)} | S_i^{(\alpha)} | \psi_j^{(\beta)}} \neq 0 ? \\
  \label{eq:5.51}
  \eqrel{eq:5.50}{=}{}& \sum_{\gamma,s,k} \braket{\psi_m^{(\varrho)} | \varphi_k^{(\gamma,s)}} \braket{\gamma s k|\alpha i, \beta j} = \sum_s \braket{\psi_m^{(\varrho)} | \varphi^{(\varrho,s)}} \braket{\varrho s m|\alpha i, \beta j}
\end{align}
Man schreibt üblicherweise
\begin{equation}
  \label{eq:5.52}
  \braket{\psi_m^{(\varrho)} | \varphi_m^{(\varrho,s)}} = \braket{\psi^{(\varrho)} || S^{(\alpha)} || \psi^{(\beta)}}_s
\end{equation}
und nennt es \acct{reduziertes Matrixelement}.  Das ist das
Wigner-Eckart-Theorem.

\begin{theorem}
  \label{thm:5.4}
  Die Abhängigkeit des Matrixelements
  \begin{equation}
    \label{eq:5.53}
    \braket{\psi_k^{(\gamma)} | S_i^{(\alpha)} | \psi_j^{(\beta)}} = \sum_s \braket{\psi^{(\gamma)} || S^{(\alpha)} || \psi^{(\beta)}}_s \braket{\gamma s k|\alpha i, \beta j}
  \end{equation}
  von den Zeilenindizes (Komponentenindizes) $i,j,k$ steht
  ausschließlich in den durch die Symmetrie bestimmten
  Clebsch-Gordan-Koeffizienten.
\end{theorem}

\minisec{Anmerkungen}

Allein die Symmetrie entscheidet durch die
Clebsch-Gordan-Koeffizienten, ob mit einem Operator aus der
Darstellung $\alpha$ ein Übergang von einem Zustand aus $\beta$ zu
einem aus $\gamma$ möglich ist.  Sie entscheidet auch, welche
Basisfunktionen $j$ aus $\beta$ mit welcher $k$ aus $\gamma$
kombinieren kann, wenn nur die Operatorkomponente $S_i^{(\alpha)}$
wirkt.

Die Stärke des Übergangs entscheidet sich durch den Wert des
Clebsch-Gordan-Koeffizienten und den Wert des reduzierten
Matrixelements, der aber nicht von den verwendeten Komponenten
abhängt.

Ist $S$ ein invarianter Operator (z.B.\ skalarer Operator), so gilt
\begin{subequations}
  \begin{align}
    \label{eq:5.54a}
    \braket{\gamma s k | \alpha i, \beta j} &= \delta_{\gamma\beta} \delta_{kj} \\
    \label{eq:5.54b}
    \braket{\psi^{(\gamma)} || S^{(\alpha)} || \psi^{(\beta)}}_s &= \delta_{\gamma\beta} c_s^{(\beta)}
  \end{align}
\end{subequations}
Spezialfall \eqref{eq:5.27}, \eqref{eq:5.28}.

\subsection{Anmerkungen}

\subsubsection{Elektrischer Dipolübergang im Zentralpotential}

Die Symmetrie des Potentials ist $\mathrm{SO}(3)$, Basisfunktionen
$\ket{n,l,m}$, irreduzible Darstellung $l$.  Die Vektordarstellung
$D^{(V)}(g)$ ist (o.B.) identisch mit der Darstellung $D^{(V)}(g)$ zu
$l=1$, also kann man für den Vektoroperator $\bm p$ im Dipoloperator
schreiben
\begin{equation}
  \label{eq:5.55}
  \bm p\cdot\bm E = -p_{-1}^{(1)} E_1^{(1)} + p_0^{(1)} E_0^{(1)} - p_1^{(1)} E_{-1}^{(1)}
\end{equation}
Zirkulare Basis: $E_0 = E_z$, $E_1 = (E_x + \ii E_y)/\sqrt{2}$,
$E_{-1} = (E_x - \ii E_y)/\sqrt{2}$.

Sei nun beispielsweise
\begin{subequations}
  \begin{align}
    \label{eq:5.56a}
    E_{-1}^{(1)} = E_0^{(1)} &= 0 \\
    \label{eq:5.56b}
    \bm p\cdot\bm E &= -p_{-1}^{(1)} E_1^{(1)}
  \end{align}
\end{subequations}
Die Matrixelemente sind also
\begin{equation}
  \label{eq:5.57}
  M \eqrel{eq:5.56b}{\sim} E_1^{(1)} \braket{n_1 l_1 m_1 | p_{-1}^{(1)} | n_2 l_2 m_2}
\end{equation}
und das Wigner-Eckart-Theorem liefert
\begin{equation}
  M \eqrel{eq:5.57}[eq:5.53]{\sim} \sum_s \underbrace{\braket{n_1 l_1 || p^{(1)} || n_2 l_2}_s}_{\text{hängt nicht von $m$ ab}} \underbrace{\braket{l_1 s_1 m_1 | 1\; {-1}, l_2 m_2}}_{(*)}
\end{equation}
$(*)$ bestimmt die Auswahlregeln und aus allgemeinen Eigenschaften der
Clebsch-Gordan-Koeffizienten folgt, dass $m_1 = m_2 - 1$ und $l_1 \in
\{ l_2-1, l_2+1 \}$, also $\Delta m = -1$ und $\Delta l = \pm 1$.

\subsubsection{Teilchen in einem Potential mit \texorpdfstring{$C_{3v}$}{C3v}-Symmetrie}

Die Eigenzustände entstammen einer der folgenden drei irreduziblen
Darstellungen.
\begin{equation*}
  \begin{array}{c@{\quad}ccc}
        & e & \{c_3,c_3^2\} & \{\sigma_v,\sigma_v',\sigma_v''\} \\
    \midrule
    A_1 & 1 &  1 &  1 \\
    A_2 & 1 &  1 & -1 \\
    E   & 2 & -1 &  0 \\
  \end{array}
\end{equation*}
Sei $\bm E = E_z \hat e_z$, das Übergangsmatrixelement ist
proportional zu $\braket{f|z|i}$.  $z$ transformiert sich mit der
Einsdarstellung und es folgt nach \eqref{5.54a} und \eqref{5.54b}
\begin{equation*}
  \braket{\gamma s k|A_1 0, B j} = \delta_{\gamma\beta} \delta_{kj}
\end{equation*}
Es sind keine Übergänge zwischen Zuständen verschiedener Darstellungen
möglich.

Sei nun $\bm E = E_x \hat e_x$ und $\braket{f|x|i}$.  Anfangszustand
sei eine Basisfunktion zu $A_1$.  Der Dipoloperator stammt aus der
Darstellung $E$.  Ein Endzustand ist möglich aus $D^{(+)} \subseteq
A_1 \otimes E$.  (Dann verschwinden die Clebsch-Gordan-Koeffizienten
nicht zwangsweise, es gibt Komponenten, für die sie nicht
verschwinden) Also möglich aus $A \otimes E = E$.

Zweites Beispiel: Der Anfangszustand transformiert sich nach $E$.  Der
Endzustand ist möglich aus $E \otimes E = A_1 \oplus A_2 \oplus E$
also beliebig.


%%% Local Variables:
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "deutsch"
%%% TeX-master: "../Gruppentheorie.tex"
%%% End:
