\subsubsection{Lagrangedichte der Quantenelektrodynamik}

Das freie Diracfeld (d.h.~die Diracgleichung) lässt sich aus der
Lagrangedichte
\begin{equation}
  \label{eq:7.17}
  \mathcal L_D = \ii\hbar \bar\psi \gamma^\mu \partial_\mu \psi - m c^2 \bar\psi \psi
\end{equation}
mit dem Dirac-adjungierten Spinor $\bar\psi = \psi^\dagger \gamma^0$
gewinnen, wenn man berechnet mit der Euler-Lagrange-Gleichung für Felder
\begin{equation}
  \label{eq:7.18}
  \frac{\partial \mathcal L_D}{\partial \psi_r} - \partial_\mu \frac{\partial \mathcal L_D}{\partial \psi_{r,\mu}} = 0 \;,\quad \psi_{r,\mu} = \frac{\partial \psi_r}{\partial x^\mu} = \partial_\mu \psi_r
\end{equation}
Nicht invariant unter der lokalen Eichtransformation \eqref{eq:7.14}.
Das gilt auch nicht für die Lagrangedichte des Maxwellfeldes
\begin{subequations}
  \begin{align}
    \label{eq:7.19a}
    \mathcal L_M &= - \frac{1}{4\mu_0} F_{\mu\nu} F^{\mu\nu} \\
    \label{eq:7.19b}
    F_{\mu\nu} &= \partial_\mu A_\nu - \partial_\nu A_\mu \quad \text{(Faraday-Tensor)}
  \end{align}
\end{subequations}
Man kann jedoch ausrechnen, dass die gemeinsamen Phasentransformationen
\begin{align}
  \tag*{\eqref{eq:7.14}}
  \psi \to \psi' &= \ee^{-\ii\frac{e}{\hbar} \lambda(x)} \psi \\
  \label{eq:7.20}
  A_\mu \to A_\mu' &\eqrel{eq:7.10a,eq:7.10b}[eq:7.13c,eq:7.13d]{=} A_\mu + \partial_\mu \lambda
\end{align}
die Lagrangedichte der Quantenelektrodynamik
\begin{align}
  \notag
  \mathcal L_{\text{QED}} &= \ii\hbar \bar\psi \gamma^\mu \partial_\mu \psi - mc^2 \bar\psi \psi - \frac{1}{4\mu_0} F_{\mu\nu} F^{\mu\nu} - c e \bar\psi \gamma^\mu \psi A_\mu \\
  \label{eq:7.21}
  &= \ii\hbar \bar\psi \gamma^\mu D_\mu \psi - mc^2 \bar\psi \psi - \frac{1}{4\mu_0} F_{\mu\nu} F^{\mu\nu}
\end{align}
invariant lassen.

Die mathematische Forderung nach einer lokalen Eichinvarianz der
Diracgleichung erzwingt die Ankopplung an das elektromagnetische Feld.
Dies hat eine physikalische Konsequenz.

\subsubsection{Gruppentheoretische Analyse}

\begin{equation*}
  g^{-1} = \ee^{-\ii q \lambda} \in \mathrm{U}(1)
\end{equation*}
ist also Element einer Liegruppe.  Die in \eqref{eq:7.8} auftretenden
Ableitungen lauten in Viererschreibweise
\begin{equation}
  \label{eq:7.22}
  \ee^{\ii q \lambda/\hbar} \partial_\mu \ee^{-\ii q \lambda/\hbar} = (\partial_\mu \lambda) \ee^{\ii q \lambda/\hbar} \frac{\diff}{\diff \lambda} \ee^{-\ii q \lambda/\hbar}
\end{equation}
Der Term
\begin{equation}
  \label{eq:7.23}
  \left. \frac{\diff}{\diff \lambda} \ee^{-\ii q \lambda/\hbar} \right|_{\lambda=\lambda_0}
\end{equation}
ist Tangentialvektor an $\mathrm{U}(1)$ bei $\ee^{-\ii q \lambda_0/\hbar}$ und
\begin{equation*}
  \left. \ee^{\ii q \lambda/\hbar} \frac{\diff}{\diff \lambda} \ee^{-\ii q \lambda/\hbar} \right|_{\lambda=\lambda_0} = \frac{\ii q}{\hbar}
\end{equation*}
ist \enquote{zur $1$ zurücktranportierter Tangentialvektor} und damit
ein Element der Liealgebra von $\mathrm{U}(1)$.  Auf Grund der
identischen Formen der Phasentransformation der Ableitung
\eqref{eq:7.8} und der für die Felder \eqref{eq:7.20} ist $-\ii q
A_\mu / \hbar$ ebenfalls Liealgebra-wertig.

\subsection{Erweiterung zu einer nichtabelschen Yang-Mills-Eichtheorie}

Historisch: Nachdem die elektromagnetische Wechselwirkung sich durch
die Forderung nach einer lokalen $\mathrm{U}(1)$-Eichinvarianz
einführen ließ, sucht man nach einer Beschreibung anderer
Wechselwirkungen auf dieselbe Art.  Für neue Kräfte ist die Ausnutzung
neuer Freiheitsgrade notwendig.

Hier exemplarisch:  Isospin
\begin{equation*}
  \up =
  \begin{pmatrix}
    1 \\ 0 \\
  \end{pmatrix}
  \;\text{Proton}
  \quad
  \dn =
  \begin{pmatrix}
    0 \\ 1 \\
  \end{pmatrix}
  \;\text{Neutron}
\end{equation*}
Beides sind Fermionen, also korrekte Beschreibung mit Diracspinoren:
\begin{equation}
  \label{eq:7.24}
  \psi =
  \begin{pmatrix}
    \psi_{\text{prot}} \\ \psi_{\text{neut}} \\
  \end{pmatrix}
  \in \mathbb C^2 \times \mathbb C^4
\end{equation}
Experimentell bekannt: Es gibt Kernreaktionen, die den
Nukleonenzustand ändern.  Wir suchen also nach einer Beschreibung für
Prozesse
\begin{equation}
  \Psi =
  \begin{pmatrix}
    \psi_{\text{prot}} \\ \psi_{\text{neut}} \\
  \end{pmatrix}
  \to
  \begin{pmatrix}
    \psi_{\text{prot}}' \\ \psi_{\text{neut}}' \\
  \end{pmatrix}
  = g^{-1}
  \begin{pmatrix}
    \psi_{\text{prot}} \\ \psi_{\text{neut}} \\
  \end{pmatrix}
\end{equation}
mit einem $g \in \mathrm{SU}(2)$, da normerhaltend.

In einer typischen Lagrangedichte für die freie Dynamik der Felder
$\Psi$ treten Terme der ersten Ableitung $\partial_\mu \Psi$ auf,
vgl.~\eqref{eq:7.17}.  Deshalb fordern wir lokale Eichinvarianz.
Diese ist erfüllt, wenn sich das für die Ableitungen erfüllen lässt.
Aus \eqref{eq:7.16} wissen wir, das es mit einer kovarianten Ableitung
funktioniert
\begin{subequations}
  \begin{equation}
    \label{eq:7.25a}
    D_\mu = \partial_\mu - \ii q A_\mu
  \end{equation}
  und deren Transformationsverhalten
  \begin{equation}
    \label{eq:7.25b}
    \tilde D_\mu = \partial_\mu - \ii q \left( A_\mu + \frac{\ii}{q} g^{-1} \partial_\mu g \right)
  \end{equation}
\end{subequations}
Die Darstellung der Gruppenelemente folgt durch Exponentialabbildung.
\begin{equation}
  \label{eq:7.26}
  g^{-1} \partial_\mu g = \ee^{\ii M} \partial_\mu \ee^{-\ii M} = - \ii \partial_\mu M + \frac{1}{2!} [M,\partial_\mu M] - \frac{\ii}{2!} [M,[M,\partial_\mu M]] - \cdots
\end{equation}
Für $\mathrm{SU}(2)$ ist $M$ eine Linearkombination der Paulimatrizen.
Die Entwicklungskoeffizienten müssen die $x$-Abhängigkeit tragen ($x =
x^\mu = (t,x,y,z)^\tp$).
\begin{subequations}
  \begin{align}
    \label{eq:7.27a}
    M &= q \lambda^a S_a \;,\quad\text{mit } S_a = \frac{\sigma_a}{2} \\
    \label{eq:7.27b}
    \partial_\mu M &\eqrel{eq:7.27a}{=} q (\partial_\mu \lambda^a) S_a
  \end{align}
\end{subequations}
Es folgt:
\begin{equation}
  \label{eq:7.28}
  g^{-1} (\partial_\mu g) \eqrel{eq:7.27a,eq:7.27b}[eq:7.26]{=} - \ii q \bigl( (\partial_\mu \lambda^a) + \text{Kommutatorterme} \bigr) S_a \in L(\mathrm{SU}(2))
\end{equation}
Sowohl $g^{-1} \partial_\mu g$ als auch $A_\mu$ sind Elemente der
Liealgebra zu $\mathrm{SU}(2)$.  Zu jedem Generator $S_a$ muss es ein
Eichfeld $A_\mu^a$ geben, also insgesamt drei.
\begin{equation}
  \label{eq:7.29}
  A_\mu = A_\mu{}^a S_a
\end{equation}
Analog zum Feldtensor \eqref{eq:7.19b} gibt es auch hier einen für den gilt:
\begin{subequations}
  \begin{align}
    \notag
    - \ii q F_{\mu\nu} &= [D_\mu,D_\nu] = - \ii q (\partial_\mu A_\nu - \partial_\nu A_\mu - \ii q [A_\mu,A_\nu]) \\
    \label{eq:7.30a}
    &= - \ii q F_{\mu\nu}{}^a S_a \\
    \notag
    F_{\mu\nu}{}^a &= \partial_\mu A_\nu{}^a - \partial_\nu A_\mu{}^a + q \varepsilon_{abc} A^b{}_\mu A_\nu{}^c \\
    \label{eq:7.30b}
    &= \partial_\mu A_\nu{}^a - \partial_\nu A_\mu{}^a + q (\bm A_\mu \times \bm A_\nu)^a
  \end{align}
  wobei
  \begin{equation*}
    \bm A_\mu =
    \begin{pmatrix}
      A_\mu{}^1 \\
      A_\mu{}^2 \\
      A_\mu{}^3 \\
    \end{pmatrix}
  \end{equation*}
\end{subequations}
Im Vergleich zum Faradaytensor \eqref{eq:7.19b} füllt der letzte Term auf Ursprung:
\begin{equation*}
  A_\mu \in L(\mathrm{SU}(2)) \to \text{kommutieren nicht}
\end{equation*}
Entscheidenende physikalische Konsequenz.

Eine lokal $\mathrm{SU}(2)$-invariante Lagrangedichte hat analog
\eqref{eq:7.21} die Form
\begin{equation*}
  \mathcal L = \ii\hbar \Psi^\dagger
  \begin{pmatrix}
    \gamma^\mu & 0 \\
    0 & \gamma^\mu \\
  \end{pmatrix}
  \partial_\mu \Psi - m c^2 \Psi^\dagger \Psi - \frac{1}{q} \sum_a F_{\mu\nu}{}^a F^{a\mu\nu}
\end{equation*}
mit $\Psi^\dagger = (\bar\psi_1,\bar\psi_2)$.  Der letzte Term enthält
nach \eqref{eq:7.30b} vierte Potenzen der Felder $A_\mu$.  Die
Bewegungsgleichungen nach \eqref{eq:7.18} enthalten somit nichtlineare
Terme in $A_\mu$.

Bedeutung: Die Felder wechselwirken mit sich selbst.  Sie tragen
selbst die Ladung an die sie koppeln.  Im Unterschied koppeln Photonen
an die elektrische Ladung, tragen diese aber nicht.

%%% Local Variables:
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "deutsch"
%%% TeX-master: "../Gruppentheorie.tex"
%%% End:
