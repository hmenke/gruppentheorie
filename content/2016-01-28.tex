Man kann aber immer eine reelle Darstellung aus zwei nicht-reellen
gewinnen (nicht mehr irreduzibel).
\begin{equation}
  \label{eq:4.48}
  \frac{1}{\sqrt{2}}
  \begin{pmatrix}
    1 & 1 \\
    -\ii & \ii \\
  \end{pmatrix}
  \begin{pmatrix}
    D & 0 \\
    0 & D^* \\
  \end{pmatrix}
  \frac{1}{\sqrt{2}}
  \begin{pmatrix}
    1 & \ii \\
    1 & -\ii \\
  \end{pmatrix}
  = \frac{1}{2}
  \begin{pmatrix}
    D+D^* & \ii(D-D^*) \\
    -\ii(D-D^*) & D+D^* \\
  \end{pmatrix}
\end{equation}

\section{Produkt von Darstellungen und Clebsch-Gordan-Reihe}

\subsection{Produktdarstellungen}

Wir betrachten Produkträume, z.B.\
\begin{align*}
  \ket{\psi} &= \psi(\bm r) \otimes \ket{\up} \\
  \psi(\bm r_1,\bm r_2,\ldots) &= \psi_1(\bm r_1) \otimes \psi_2(\bm r_2) \otimes \cdots
\end{align*}
Allgemein
\begin{equation}
  \label{eq:4.49}
  \bm x = \sum_{i,j} x_{ij} \bm v_i \otimes \bm w_j \in V\otimes W
\end{equation}
Auf diese Vektoren wirkt nun eine Produktdarstellung.

\begin{theorem}
  \label{thm:4.11}
  Sei $D^{(\alpha)}\colon\mathcal G\to\mathrm{GL}(V)$ eine irreduzible
  Darstellung von $\mathcal G$ mit Basis $\{\bm
  v_1^{(\alpha)},\dotsc,\bm v_n^{(\alpha)}\}$ und
  $D^{(\beta)}\colon\mathcal G\to\mathrm{GL}(W)$ eine solche mit Basis
  $\{\bm w_1^{(\beta)},\dotsc,\bm v_m^{(\beta)}\}$. Dann ist die
  Produktdarstellung
  \begin{equation}
    \label{eq:4.50}
    D^{(\alpha\otimes\beta)} = D^{(\alpha)} \otimes D^{(\beta)} \colon \mathcal G \to \mathrm{GL}(V\otimes W)
  \end{equation}
  eine i.a.\ reduzible Darstellung auf dem Produktraum $V\otimes W$
  mit Basis $\{\bm v_i^{(\alpha)}\otimes \bm w_j^{(\beta)}\}$ und den
  Matrixelementen
  \begin{equation}
    \label{eq:4.51}\relax
    [ D^{(\alpha)}(g) \otimes D^{(\beta)}(g)]_{rs,ij} = D^{(\alpha)}_{ri}(g) D^{(\beta)}_{sj}(g)
  \end{equation}
  Name: direkte Produktdarstellung.  Charaktere
  \begin{equation}
    \label{eq:4.52}
    \chi^{(\alpha\otimes\beta)} \eqrel{eq:4.51}{=} \sum_{i,j} D^{(\alpha)}_{ii}(g) D^{(\beta)}_{jj}(g)
    = \chi^{(\alpha)}(g) \chi^{(\beta)}(g)
  \end{equation}
\end{theorem}

\subsection{Clebsch-Gordan-Reihe}

Die Produktbasis ist nicht die einzig mögliche.  Wechselt man zu einer
neuen, kann die Produktdarstellung in eine direkte Summe irreduzibler
Darstellungen zerlegt werden.
\begin{subequations}
  \begin{equation}
    \label{eq:4.53a}
    D^{(\alpha)} \otimes D^{(\beta)} = \bigoplus_\gamma (\alpha\beta|\gamma) D^{(\gamma)}
  \end{equation}
  mit den Reduktionskoeffizienten
  \begin{equation}
    \label{eq:4.53b}
    (\alpha\beta|\gamma) = \braket{\chi^{(\alpha)}\chi^{(\beta)}|\chi^{(\gamma)}}
    = \frac{1}{\lvert\mathcal G\rvert} \sum_{g\in\mathcal G} \chi^{(\alpha)}(g)^* \chi^{(\beta)}(g)^* \chi^{(\gamma)}(g)
  \end{equation}
  Man sieht sofort 
  \begin{equation}
    \label{eq:4.53c}
    (\alpha\beta|\gamma) = (\beta\alpha|\gamma)
  \end{equation}
\end{subequations}
Daraus folgt, dass es zwei Formen von Darstellungsmatrizen
$D^{(\alpha\otimes\beta)}$ gibt
\begin{enumerate}
\item in der natürlichen Basis $\{ \bm v_i^{(\alpha)} \otimes \bm w_j^{(\beta)} \}$.
\item in der ausreduzierten Basis (Blockform für irreduzible Anteile).
\end{enumerate}
nach unitärer Transformation \eqref{eq:4.53a}.  Man bezeichnet die
Basisvektoren mit
\begin{equation}
  \label{eq:4.54}
  \bm u_k^{(\gamma,s)} \;,\quad
  s=1,2,\dots,\underbrace{(\alpha\beta|\gamma)}_{\mathclap{\text{Vielfachheit der Darstellung}}} \;,\quad
  k=1,\dots,\overbrace{D^{(\gamma)}}^{\mathclap{\text{Anzahl notw.\ Basisvektoren für die Darstellung $\gamma$}}} \;.
\end{equation}
Die Basistransformation lautet
\begin{equation}
  \label{eq:4.55}
  \bm u_k^{(\gamma,s)} = \sum_{\alpha,i,\beta,j} \bm v^{(\alpha)}_i \otimes \bm w^{(\beta)}
  \braket{\alpha i,\beta j|\gamma sk}
\end{equation}
mit den Clebsch-Gordan-Koeffizienten $\braket{\alpha i,\beta j|\gamma
  sk}$.  Die Umkehrung lautet
\begin{equation}
  \label{eq:4.56}
  \bm v^{(\alpha)}_i \otimes \bm w^{(\beta)}_j = \sum_{\gamma,s,k} \bm u_k^{(\gamma,s)} \braket{\gamma sk|\alpha i,\beta j} \;.
\end{equation}

Besser vertraut aus der Quantenmechanik ist wahrscheinlich die
Bra-Ket-Notation
\begin{subequations}
  \begin{align}
    \label{eq:4.57a}
    \ket{\gamma sk} &= \sum_{\alpha i,\beta j} \ket{\alpha i} \ket{\beta j} \braket{\alpha i,\beta j|\gamma sk} \\
    \label{eq:4.57b}
    \ket{\alpha i}\ket{\beta j} &= \sum_{\gamma,s,k} \ket{\gamma sk} \braket{\gamma sk|\alpha i,\beta j}
  \end{align}
\end{subequations}
Relationen bei Orthonormalbasen:
\begin{subequations}
  \begin{align}
    \label{eq:4.58a}
    \sum_{\gamma,s,k} \braket{\alpha i,\beta j|\gamma s k}\braket{\gamma s k|\alpha' i',\beta' j'}
    &= \delta_{ii'} \delta_{jj'} \delta_{\alpha\alpha'} \delta_{\beta\beta'} \\
    \label{eq:4.58b}
    \sum_{\alpha i,\beta j} \braket{\gamma s k|\alpha i,\beta j}\braket{\alpha i,\beta j|\gamma' s' k'}
    &= \delta_{\gamma\gamma'} \delta_{ss'} \delta_{kk'}
  \end{align}
\end{subequations}
\begin{equation}
  \label{eq:4.59}
  \braket{\gamma s k|\alpha i, \beta j} = \braket{\alpha i, \beta j|\gamma s k}
\end{equation}

\section{Methode der Projektionsoperatoren}

Bisher wissen wir nur, dass Darstellungen in ihre irreduziblen Anteile
zerlegt werden können.  Nun wollen wir betrachten, wie das geht.

\begin{notice}[Anwendungen:]
  \begin{enumerate}
  \item Finden aller irreduziblen Darstellungen einer Gruppe aus der
    regulären Darstellung, vgl.\ Abschnitt~\ref{sec:4.4.5}.
  \item Berechnen der Clebsch-Gordan-Koeffizienten in \eqref{eq:4.55}.
  \end{enumerate}
\end{notice}

\subsection{Projektionsoperatoren}

Sei $V$ ein unitärer Vektorraum und $V = W \oplus W^\bot$.  Für $\bm v
\in V$ gilt die eindeutige Zerlegung $\bm v = \bm w + \bm w^\bot$ mit
$\bm w \in W$, $\bm w^\bot \in W^\bot$.  Der Operator
\begin{equation}
  \label{eq:4.60}
  \begin{aligned}
    \mathcal P\colon V &\to V\\
    \mathcal P\bm v &= \bm w
  \end{aligned}
\end{equation}
heißt Projektionsoperator.

\begin{theorem}
  \label{thm:4.12}
  Für den Projektionsoperator $\mathcal P$ gilt $\mathcal P^2 =
  \mathcal P$ (Idempotenz), $\mathcal P = \mathcal P^\dagger$
  (selbstadjungiert), $\im\mathcal P = W$, $\ker\mathcal P = W^\bot$,
  $\mathcal P + \mathcal P^\bot = 1$ mit $\mathcal P^\bot \bm v = \bm
  w^\bot$.

  Ist umgekehrt ein idempotenter, selbstadjungierter Operator
  $\mathcal P\colon V \to V$ gegeben, so gilt $V = \im\mathcal
  P\oplus\ker\mathcal P$, $\im\mathcal P \bot \ker\mathcal P$.
\end{theorem}

\subsection{Projektionen und Darstellungen}

\begin{theorem}
  \label{thm:4.13}
  Sei $D$ eine endlichdimensionale Darstellung einer Gruppe $\mathcal
  G$ auf dem unitären Vektorraum $V$, der in die invarianten Teilräume
  $W_1$ und $W_2$ zerfällt.  Sei $\mathcal P$ ein Projektionsoperator
  auf $W_1$.  Dann gilt für alle $g\in\mathcal G$
  \begin{equation}
    \label{eq:4.61}
    D(g) \mathcal P = \mathcal P D(g) \;,\quad [D(g),\mathcal P] = 0
  \end{equation}
\end{theorem}

\begin{theorem}
  \label{thm:4.14}
  Sei $D$ eine endlichdimensionale Darstellung einer Gruppe $\mathcal
  G$ auf dem unitären Vektorraum $V$ und $\mathcal P\colon W\to W$ ein
  Projektionsoperator mit $[D(g),\mathcal P]=0$ für alle $g\in\mathcal
  G$, so bilden $\im\mathcal P$ und $\ker\mathcal P$ invariante
  Teilräume von $V$.
\end{theorem}

\begin{theorem}
  \label{thm:4.15}
  Sei $W$ ein invarianter Teilraum und $\mathcal P$ ein
  Projektionsoperator auf $W$ mit $[D(g),\mathcal P]=0$ für alle $g
  \in\mathcal G$.  $D$ ist genau dann auf $W$ irreduzibel, wenn
  $\mathcal P$ nicht in Projektoren $\mathcal P_1$, $\mathcal P_2$ mit
  $\mathcal P=\mathcal P_1 + \mathcal P_2$, $[D(g),\mathcal P_i]=0$,
  $i=1,2$, $g\in\mathcal G$, $\mathcal P_1\mathcal P_2 = \mathcal
  P_2\mathcal P_1 = 0$, $\mathcal P_i \neq 0$ zerlegt werden kann.
\end{theorem}


%%% Local Variables:
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "deutsch"
%%% TeX-master: "../Gruppentheorie.tex"
%%% End:
