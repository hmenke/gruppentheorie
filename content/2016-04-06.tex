\chapter{Anwendungen von Gruppen und deren Darstellungen in der Physik}

\section{Entartungen in Spektren}

\subsection{Die Symmetrie eines Hamiltonoperators}

Energieniveaus sind Eigenwerte der zeitunabhängigen
Schrödingergleichung.
\begin{equation}
  \label{eq:5.1}
  H \ket{\psi} = E \ket{\psi}
  \quad\text{mit}\quad \ket{\psi} \in V = \text{Hilbertraum}
\end{equation}

Sei $\mathcal G$ eine Gruppe, die auf den Hilbertraum $V$ wirkt.
Diese Wirkung ist leicht zu bestimmen, wenn man die Wirkung der Gruppe
auf den Ortsraum kennt (meist Symmetrieoperationen).
\begin{align}
  \braket{\bm r|D(g)|\psi_i}
  \notag
  &= (D(g)\psi_i)(\bm r)
  = \braket{D^\dagger(g) \bm r|\psi_i}
  = \braket{\bm R^\dagger(g) \bm r|\psi_i} \\
  \notag
  &= \braket{\bm R^{-1}(g) \bm r|\psi_i}
  = \psi_i(\bm R^{-1}(g) \bm r)
  = \psi_i(\bm R(g^{-1}) \bm r) \\
  \label{eq:5.2}
  &= \sum_j D_{ij}(g) \psi_j(\bm r)
\end{align}
Dies legt die Gruppenelemente $D_{ij}$ eindeutig fest.

Ist $\mathcal G$ Symmetrie des Hamiltonoperators
(Definition~\ref{def:2.9}) muss gelten
\begin{equation}
  \label{eq:5.3}
  H D(g) = D(g) H \quad\text{oder}\quad
  D(g) H D^{-1}(g) = H \quad\text{oder}\quad
  [H,D(g)] = 0 \quad\text{für alle $g\in\mathcal G$.}
\end{equation}
und damit
\begin{equation}
  \label{eq:5.4}
  H D(g) \ket{\psi_{E_i}} = D(g) H \ket{\psi_{E_i}} = E D(g) \ket{\psi_{E_i}}
  \;,\quad i \hateq \text{Entartungsindex.}
\end{equation}

\subsection{Invariante Unterräume und Entartung}

\begin{theorem}
  \label{thm:5.1}
  Sei der Hamiltonoperator $H$ invariant unter einer Gruppe $\mathcal
  G$, die auf seinen Hilbertraum $V$ wirkt.  Sei $V^{(\alpha)}$ ein
  echter invarianter Unterraum von $V$ zur irreduziblen Darstellung
  $D^{(\alpha)}$.
\end{theorem}

Dann gilt in $V^{(\alpha)}$ wegen \eqref{eq:5.3}:
\begin{equation*}
  D^{(\alpha)}(g) H = H D^{(\alpha)}(g) \quad \forall g \in \mathcal G \; .
\end{equation*}
Nach dem Schur-Lemma (Satz~\ref{thm:4.3}) folgt in $V^{(\alpha)}$
\begin{equation*}
  H = \lambda_\alpha \id_{V^{(\alpha)}} \quad\text{mit\ } \lambda_\alpha \in \mathbb C \;.
\end{equation*}
D.h.\ $V^{(\alpha)}$ ist ein Eigenraum von $H$ zum Eigenwert
$\lambda_\alpha$.  Der Entartungsgrad ist $d_\alpha = \gr
D^{(\alpha)}$.

\begin{theorem}
  \label{thm:5.2}
  Sei $W_E$ Eigenraum zum Hamiltonoperator $H$ mit Eigenwert $E$ und
  Basis $\{ \ket{\psi_1},\dots,\ket{\psi_n} \}$, d.h.\ $H \ket{\psi_i}
  = E \ket{\psi_i}$, $i = 1,\dots,n$.  Es wirke eine Gruppe $\mathcal
  G$ auf den Hilbertraum unter der $H$ invariant ist.
\end{theorem}

Dann gilt:
\begin{equation*}
  \tag{\eqref{eq:5.4}}
  H D(g) \ket{\psi_i}= D(g) H \ket{\psi_i} = E D(g) \ket{\psi_i} \;.
\end{equation*}
Somit $D(g) \ket{\psi_i} \in W_E$ und kann nach dessen Basis
entwickelt werden
\begin{equation}
  \label{eq:5.5}
  D(g) \ket{\psi_i} = \sum_{j=1}^{n} \ket{\psi_i} D_{ij}(g) \; .
\end{equation}
D.h.\ $W_E$ ist invarianter Unterraum zu $\mathcal G$ und $\{
\ket{\psi_i} \}$ ist Basis zu einer Darstellung von $\mathcal G$ mit
Darstellungsmatrix $\bm D$.

Wichtige Unterscheidung
\begin{enumerate}
\item $W_E$ ist echter invarianter Unterraum, also $W_E =
  V^{(\alpha)}$ nach Satz~\ref{thm:5.1}.  Die Entartung ist
  \acct*{symmetriebedingt} und gilt immer, solange $[D(g),H] = 0$ für
  alle $g \in \mathcal G$.  Der maximale Entartungsgrad eines
  Energieniveaus ist $d_{\max} = \gr D^{\max}$, wobei $D^{\max}$ die
  irreduzible Darstellung von $\mathcal G$ mit der maximalen Dimension
  ist.
\item $W_E$ ist reduzibel, also $W_E = \bigoplus_\alpha n_\alpha
  V^{(\alpha)}$.  Zufällige Entartung (zweimal der gleiche Eigenwert
  $\lambda_1$)
  \begin{equation*}
    H = \begin{pmatrix}
      \lambda_1 D^{(1)} \\
      &\lambda_1 D^{(2)} \\
      &&\lambda_3 D^{(3)} \\
    \end{pmatrix} \; .
  \end{equation*}
  Beispiel: Atom im Magnetfeld
  \begin{center}
    \begin{tikzpicture}[gfx]
      \draw[<->] (0,3) node[left] {$E$} |- (3,0) node[below] {$B$};
      \draw (0,1) -- (3,2);
      \draw (0,1) -- (3,0);
      \draw (0,2) -- (3,1);
      \draw (0,2) -- (3,3);
      \node[MidnightBlue,dot,pin={%
        [MidnightBlue]below:Zufällige Entartung bei genau einem Wert $B$
      }] at (1.5,1.5) {};
    \end{tikzpicture}
  \end{center}
\end{enumerate}


\subsection{Standardbeispiel: Teilchen im Zentralpotential}

\subsubsection{Hamiltonoperator}

\begin{equation}
  \label{eq:5.6}
  H = \frac{p^2}{2 m} + V(\lvert\bm r\rvert)
\end{equation}
dann
\begin{equation}
  \label{eq:5.7}\relax
  [H,l_i] = 0
\end{equation}
wobei $l_i$ die Komponenten des Drehimpulses sind mit
\begin{equation}
  \label{eq:5.8}\relax
  [l_i,l_j] = \ii\hbar\varepsilon_{ijk} l_k \;.
\end{equation}
Dann vertauscht $H$ aber auch mit den Operatoren
\begin{equation}
  \label{eq:5.9}
  R(\hat n, \varphi) = \ee^{-\ii \hat n \bm l \varphi/\hbar} \;.
\end{equation}
Nach \eqref{eq:3.12} sind dies die Generatoren von
\begin{equation*}
  \mathrm{SO}(3) = \mathrm{SU}(2)/\mathbb Z_2\;.
\end{equation*}
\begin{subequations}
  Symmetriegruppe von $H$:
  \begin{equation}
    \label{eq:5.10a}
    \mathrm{SO}(3) = \mathrm{SU}(2)/\mathbb Z_2\;.
  \end{equation}
  oder, weil $i$ Symmetrie ist
  \begin{equation}
    \label{eq:5.10b}
    \mathrm{O}(3) = \mathrm{SO}(3) \times i
  \end{equation}
\end{subequations}

\subsubsection{Irreduzible Darstellungen im Hilbertraum \texorpdfstring{von \eqref{eq:5.6}}{}}

Wissen aus der Quantenmechanik:  Hamiltonoperator umschreiben in
\begin{equation}
  \label{eq:5.11}
  H = -\frac{\hbar^2}{2m} \left( \frac{\diff^2}{\diff r^2} + \frac{2}{r} \frac{\diff}{\diff r} \right)
  + \frac{\bm l^2}{2 m r^2} + V(\lvert\bm r\rvert)
\end{equation}
Es gibt gemeinsame Eigenzustände zu $\bm l^2$ und $l_z$: $\ket{lm}$.
\eqref{eq:5.11} hängt nicht von $m$ ab, aber von $l$.  Also hängen
auch die Energien von $l$ ab: $E_{nl}$.

Folglich: Invariante Unterräume werden von allen $m$ zu einem $l$
aufgespannt
\begin{itemize}
\item 1-dimensional: $E_{n0} \ket{0,0}$
\item 3-dimensional: $E_{n1} \ket{1,-1}, \ket{1,0}, \ket{1,1}$
\item 5-dimensional: $E_{n2} \ket{2,-2}, \ket{2,-1}, \ket{2,0}, \ket{2,1}, \ket{2,2}$
\end{itemize}
Es gibt also 1-, 3-, 5-, \dots dimensionale Darstellungen.
\begin{equation}
  \label{eq:5.12}
  H = \begin{pmatrix}
    E_{n0} \\
    & E_{n1} \mathds 1_{3\times3} \\
    && E_{n2} \mathds 1_{3\times3} \\
    &&& \ddots \\
  \end{pmatrix}
  \quad
  \vcenter{\hbox{
    \begin{tikzpicture}
      \draw[->] (0,0) -- (0,2) node[right] {$E$};
      \draw (-0.1,0.0) -- +(-1,0) node[left] {$1s$};
      \draw (-0.1,0.5) -- +(-1,0) node[left] {$2s$};
      \draw (-0.1,1.0) -- +(-1,0) node[left] {$2p$};
      \draw (-0.1,1.5) -- +(-1,0) node[left] {$3s$};
    \end{tikzpicture}
  }}
\end{equation}

\subsubsection{Spezialfall Coulomb-Potential}

$V = \alpha/r$ hat kein Spektrum der Form \eqref{eq:5.12} und hängt
nicht von $l$ ab
\begin{equation*}
  H = \begin{pmatrix}
    E_{nl} \mathds 1_{n_1} \\
    & E_{nl} \mathds 1_{n_2} \\
    && \ddots \\
  \end{pmatrix}
  \quad
  \vcenter{\hbox{
    \begin{tikzpicture}
      \draw[->] (0,0) -- (0,2) node[right] {$E$};
      \draw (-0.1,0.0) -- +(-1,0) node[left] {$1s$};
      \draw (-0.1,0.5) -- +(-1,0) node[left] {$2s2p$};
      \draw (-0.1,1.5) -- +(-1,0) node[left] {$3s3p3d$};
    \end{tikzpicture}
  }}
\end{equation*}
Zufällige Entartung?  Nein!  Es liegt eine höhere Symmetrie vor.
Zusätzliche Erhaltungsgröße ist der Runge-Lenz-Operator:
\begin{equation}
  \label{eq:5.13}
  \bm\Lambda = \frac{1}{2\alpha m} (\bm p \times \bm l - \bm l \times \bm p)
    - \frac{\bm r}{\lvert\bm r\rvert}
\end{equation}
Es gilt nämlich
\begin{equation}
  \label{eq:5.14}\relax
  [H,\Lambda_i] = 0
\end{equation}
Welche Symmetrie liegt vor?  Dazu führt man ein:
\begin{subequations}
  \begin{align}
    \label{eq:5.15a}
    \bm J_1 &= \frac12 \left( \bm l + \frac{\Lambda}{\sqrt{-2 H/m}} \right) \\
    \label{eq:5.15b}
    \bm J_2 &= \frac12 \left( \bm l - \frac{\Lambda}{\sqrt{-2 H/m}} \right)
  \end{align}
\end{subequations}
\begin{subequations}
  \begin{align}
    \label{eq:5.16a}\relax
    [J_{1,i}, J_{1,j}] &= \ii\hbar\varepsilon_{ijk} J_{1,k} \\
    \label{eq:5.16b}\relax
    [J_{2,i}, J_{2,j}] &= \ii\hbar\varepsilon_{ijk} J_{2,k} \\
    \label{eq:5.16c}\relax
    [J_{1,i}, J_{2,j}] &= 0
  \end{align}
\end{subequations}
Jeder der Vektoren $\bm J_1$, $\bm J_2$ bildet offensichtlich eine
$\mathrm{SU}(2)$-Algebra, beide kommutieren.  Die Symmetriegruppe des
H-Atoms ist also
\begin{equation}
  \label{eq:5.17}
  i \times \mathrm{SU}(2) \times \mathrm{SU}(2) \cong \mathrm{O}(4) \;.
\end{equation}

\subsection{Weiteres Beispiel (Teilchen im Kristallfeld)}

Ausgangssituation: Atom im freien Raum $\mathrm{O}(3)$-Symmetrie.
Wird in einen Kristall mit kubischer Symmetrie eingebunden.  Die
Symmetriegruppe ist nun $\mathcal O < \mathrm{O}(3)$.

Aus Tabellen entnimmt man, dass es folgende irreduzible Darstellungen
gibt:
\begin{align*}
  A_1 &\colon \text{Grad 1} \\
  A_2 &\colon \text{Grad 1} \\
  E   &\colon \text{Grad 2} \\
  F_1 &\colon \text{Grad 3} \\
  F_2 &\colon \text{Grad 3}
\end{align*}


%%% Local Variables:
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "deutsch"
%%% TeX-master: "../Gruppentheorie.tex"
%%% End:
