Allgemein gilt: Sei
\begin{equation*}
  \mathcal G = \{ e,g_1,g_2,\dotsc,g_{n-1} \} \;,
\end{equation*}
dann ist
\begin{equation}
  \label{eq:3.38}
  \bar{\mathcal G} = \{ e,g_1,\dotsc,g_{n-1},\bar e, g_1 \bar e, g2 \bar e,\dotsc, g_{n-1} \bar e \} \;.
\end{equation}

\begin{notice}[Beachte:]
  $\mathcal H \nless \bar{\mathcal H}$, da $\mathrm{SO}(3) \nless \mathrm{SU}(2)$.
\end{notice}

Bildungsregeln für uneigentliche Punktgruppen funktionieren.

\section{Kristallographische Gruppen}

Als Kristall bezeichnet man eine periodisch unendlich ausgedehnte
Struktur mit Translationsanteilen in der Symmetriegruppe.

\subsection{Diskrete Translationen; Gittergruppen}

\begin{definition}
  \label{def:3.3}
  Eine \acct{Gittergruppe} ist eine nichttriviale diskrete Untergruppe
  von $\mathrm{T}(3) \cong (\mathbb R^3,+)$ des $\mathbb R^3$.
\end{definition}

Nichttrivial heißt, dass die Gruppe nicht nur aus dem neutralen
Element besteht.  $\mathcal H$ sind Mengen von Vektoren mit der
Vektoraddition als Gruppenprodukt.  Je nach Zahl der linear
unabhängigen Vektoren in drei, zwei oder einer Dimension.  Hier
betrachten wir nur 3d.  Es gibt also für uns drei linear unabhängige
Basisvektoren $\bm b_i$ in die jeder Translationsvektor zerlegt werden
kann
\begin{equation}
  \label{eq:3.39}
  \bm a = n_1 \bm b_1 + n_2 \bm b_2 + n_3 \bm b_3
  \;,\quad n_i \in \mathbb Z \;.
\end{equation}

Die $\bm b_i$ spannen die \acct{primitive Zelle} der Gittergruppe auf.
Sie sind nicht eindeutig.

\begin{definition}
  \label{def:3.4}
  Sei $\bm x \in \mathbb R^3$ ein Element des affinen Raumes und
  $\mathcal H$ eine Gittergruppe.  Dann bildet der Orbit $\mathcal
  H\bm x$ ein geometrisches Gitter.  Die Menge aller Orbits heißt
  Kristall- oder Raumgitter.
  \begin{equation*}
    \mathbb R^3/\mathcal H \cong \mathrm{T}^3 = (S^1)^3
  \end{equation*}
  heißt Fundamentalbereich.  Eine spezielle Konstruktion davon ist die
  Wigner-Seitz-Zelle.
\end{definition}

\subsection{Kristallographische Punktgruppen}

Ein Gitter $L = \mathcal H\bm x$ wird als Orbit der Gittergruppe
$\mathcal H$ gebildet.  $\mathcal H$ ist somit Untergruppe der vollen
Symmetriegruppe $\mathcal G$ des Gitters, wobei gilt
\begin{equation}
  \label{eq:3.40}
  \mathcal H = \mathcal G \cap \mathrm{T}(3) \;.
\end{equation}
Darüber hinaus enthält $\mathcal G$ noch Elemente einer diskreten
Untergruppe $\mathcal F$ von $\mathrm{O}(3)$ und es gilt
\begin{equation}
  \label{eq:3.41}
  \mathcal G = \mathcal F \wedge \mathcal H \;.
\end{equation}
Dies führt auf
\begin{definition}
  \label{def:3.5}
  Eine Untergruppe $\mathcal F < \mathrm{E}(3)$, die ein Gitter auf
  sich selbst abbildet und $\bm x \in L$ fix hält heißt
  \acct{kristallographische Punktgruppe}.  Die maximale
  kristallographische Punktgruppe heißt Holoedrie von $L$ bei $\bm x$.
\end{definition}

\begin{theorem}
  \label{thm:3.4}
  (Satz von der kristallographischen Beschränkung) Eine
  kristallographische Punktgruppe kann nur Drehachsen $c_n$ der
  Zähligkeit $n=1,2,3,4,6$ enthalten. Holoedrien enthalten stets die
  Inversion $i$ und, sofern sie eine zyklische Untergruppe $\mathcal
  C_n$ enthalten, auch $c_{nv}$.
\end{theorem}

\begin{theorem}
  \label{thm:3.5}
  Es gibt 32 kristallographische Punktgruppen und zugehörige
  Kristallklassen, sowie sieben Holoedrien.  Die Holoedrien sind:
  \begin{center}
    \missingfigure{}
  \end{center}
\end{theorem}

\begin{definition}
  \label{def:3.6}
  Zwei Gitter $L'$ und $L$ gehören zum selben \acct{Kristallsystem},
  wenn ihre Holoedrien $\mathcal F'$ und $\mathcal F$ konjugierte
  Untergruppen in $\mathrm{E}(3)$ sind.  Es gibt sieben
  Kristallsysteme.

  Damit sind bisher nur die Punktgruppen festgelegt.  Es sind noch
  mehrere Raumgruppen möglich.
\end{definition}

\begin{definition}
  \label{def:3.7}
  Zwei Gitter $L_0$ und $L_1$ derselben Holoedrie $\mathcal F$ sind
  vom gleichen \acct{Gittertyp}, wenn das eine aus dem anderen durch
  eine stetige Deformation $L_t$ mit $0 \leq t \leq 1$ erreicht wird,
  wobei die Holoedrie $\mathcal F_t$ stets $\mathcal F$ enthält.
\end{definition}

\begin{theorem}
  \label{thm:3.6}
  In drei Dimensionen gibt es 14 Gittertypen, die Bravaisgitter
  genannt werden, in zwei Dimensionen gibt es 5.
\end{theorem}

\subsection{Raumgruppen}

Wir haben geshen, dass ein Gitter eine Translationssymmetrie in Form
der Gittergruppe $\mathcal H$ hat.  Ein Gitter hat Symmetrien aus eine
Punktgruppe $\mathcal F$.  Die maximale Symmetrie wird erreicht, wenn
jeder Gitterpunkt diese nicht einschränkt: 7 Holoedrien.

Die Symmetrie kann eingeschränkt werden und alle 32
kristallographischen Punktgruppen können auftreten.

Der Kristall ist ein Gitter mit einer Basis, also ein unendlich
ausgedehntes, periodisch aufgebautes Medium mit physikalisch
äquivalenten Punkte, also solchen, die aus einer Gittertranslation
ineinander übergehen.

\begin{definition}
  \label{def:3.8}
  Eine \acct{Raumgruppe} $\mathcal G$ ist eine diskrete Untergruppe
  von $\mathrm{E}(3)$, wobei $\mathcal H = \mathcal G \cap
  \mathrm{T}(3)$ eine dreidimensionale Gittergruppe darstellt.
\end{definition}

Dazu einige Anmerkungen:
\begin{itemize}
\item $\mathcal H$ ist Normalteiler in $\mathcal G$.
\item $\mathcal H\bm x$ mit einem $\bm x \in \mathbb R^3$ ist das Kristallgitter.
\item Die Holoedrie $\mathcal F$ zum Gitter ist Symmetriegruppe des
  Gitters, nicht aber notwendigerweise des Kristalls. (Bsp: Basis
  verhindert Inversion)
\item Es gibt Symmetrieelemente in $\mathcal G$, deren
  Translationsanteil nicht zur Gittergruppe gehört
  \begin{equation*}
    \{\bm R,\bm a\} \in \mathcal G \not\implies  \{ \mathds 1,\bm a \} \in \mathcal G \;.
  \end{equation*}
  Beispiele: Gleitspiegelung, Schraubung.
\item Die Punktgruppe des Kristalls ist
  \begin{equation*}
    \mathcal K = \{ \bm R = \{ \bm R, 0 \} \mid \{ \bm R, \bm a \} \in \mathcal G \text{, für} a \in \mathrm{T}(3) \text{ beliebig} \} \cong \mathcal G/\mathcal H
  \end{equation*}
  aber im Allgemeinen $\mathcal K \nless \mathcal G$.
\item Basisvektoren $\bm b$ sind in der Größenordnug von
  \si{\angstrom}, spielen also für die makroskopische Betrachtung
  keine Rolle.  Die Punktsymmetrie ist entscheidend.
\item Es gibt 219 Isomorphieklassen der Raumgruppen, wobei
  \begin{itemize}
  \item symmorphe Raumgruppen $\mathcal K < \mathcal G$, also
    $\mathcal G = \mathcal K \wedge \mathcal H$.  73 Isomorphieklassen
  \item nichtsymmorphe Raumgruppen enthalten Schraubungen und
    Gleitspiegelungen also Elemente $\{ \bm R,\bm a \}$ für die gilt
    \begin{equation*}
      \bm a \in \mathcal H \quad \mathcal K \nless \mathcal G
    \end{equation*}
    146 Isomorphieklassen.
  \end{itemize}
\item 11 Isomorphieklassen enthalten Paare mit entgegengesetztem
  Schraubsinn, welche unterschiedliche physikalische Eigenschaften
  haben können.  230 Raumgruppen (statt 219).
\end{itemize}


%%% Local Variables:
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "de_DE"
%%% TeX-master: "../Gruppentheorie.tex"
%%% End: