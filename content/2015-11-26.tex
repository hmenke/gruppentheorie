\chapter{Beispiele für Gruppen und deren Anwendungen}

Das Ziel ist ein Verständnis für die Symmetriegruppen von Atomen,
Molekülen und Kristallen zu erlangen.

\section{Zur Drehgruppe \texorpdfstring{$\mathrm{O}(3)$}{O(3)}}

Die $\mathrm O(3)$ besteht aus allen längen- und winkelerhaltenden
Automorphismen des euklidischen Raumes $\mathbb R^3$, also
\begin{align}
  \label{eq:3.1}
  \mathrm O(3) = \Aut(\mathbb R^3,\cdot) \;, \\
  \label{eq:3.2}
  \mathrm O(3) = \{ \bm R \in \mathbb R^3, \bm R^\tp \bm R = \mathds 1 \} \;.
\end{align}
Man berechnet leicht
\begin{equation}
  \label{eq:3.3}
  \det(\bm R^\tp \bm R) = (\det\bm R)^2 = 1
  \quad\text{also}\quad
  \det\bm R = \pm 1 \;.
\end{equation}
Es gibt offensichtlich einen Homomorphismus
\begin{subequations}
  \begin{equation}
    \label{eq:3.4a}
    \begin{aligned}
      \det\colon \mathrm O(3) &\to \{ -1,1 \} \\
      \bm R &\mapsto \det\bm R
    \end{aligned}
  \end{equation}
  mit
  \begin{equation}
    \label{eq:3.4b}
    \ker\det = \mathrm{SO}(3)
  \end{equation}
  und der Faktorgruppe
  \begin{equation}
    \label{eq:3.4c}
    \mathrm{O}(3)/\mathrm{SO}(3) = \mathbb Z_2 \;.
  \end{equation}
\end{subequations}
Bezüglich der Untergruppe $\mathrm{SO}(3)$ gibt es zwei
Linksnebenklassen
\begin{align*}
  e \mathrm{SO}(3) &= \mathrm{SO}(3) = \text{eigentliche Drehungen} \\
  i \mathrm{SO}(3) &= \text{uneigentliche Drehungen}
\end{align*}
mit der Inversion
\begin{equation}
  \label{eq:3.5}
  i =
  \begin{pmatrix}
    -1 \\
    & -1 \\
    && -1 \\
  \end{pmatrix} \;.
\end{equation}

\subsection{Zur Drehgruppe \texorpdfstring{$\mathrm{SO}(3)$}{SO(3)}}

Welche Zusammenhänge bestehen zwischen den Matrizen $\bm R$ und der
Drehachse sowie dem Drehwinkel?

Eine einfache Aussage dazu ist der folgende Satz.
\begin{theorem}
  \label{thm:3.1}
  Sei $\bm R \in \mathrm{SO}(3)$ mit $\bm R \neq \mathds 1$.  $\bm R$ besitzt
  drei Eigenwerte, nämlich $1$, $\ee^{\ii\varphi}$ und
  $\ee^{-\ii\varphi}$, und beschreibt eine Drehung um den Eigenvektor
  $1$ (Fixpunkt, Drehachse) mit dem Winkel $\varphi$,
  vgl.~\eqref{eq:1.8}.
\end{theorem}

Im Hauptachsensystem heißt das
\begin{equation}
  \label{eq:3.6}
  \begin{pmatrix}
    \cos\varphi & - \sin\varphi & 0 \\
    \sin\varphi & \cos\varphi & 0 \\
    0 & 0 & 1 \\
  \end{pmatrix} \;.
\end{equation}
In einem beliebigen Koordinatensystem findet man mit der Drehachse
$\hat n$ und dem Drehwinkel $\varphi$
\begin{align}
  \notag
  \bm R &= \mathds 1\cos\varphi + (\mathds 1-\cos\varphi) \hat n \otimes \hat n
  + \sin\varphi
  \begin{pmatrix}
    0 & -n_3 & n_2 \\
    n_3 & 0 & -n_1 \\
    -n_2 & n_1 & 0 \\
  \end{pmatrix} \\
  \label{eq:3.7}
  &= \mathds 1\cos\varphi + (\mathds 1-\cos\varphi) \hat n \otimes \hat n
  + \sin\varphi \hat n \times \;.
\end{align}
Eine einfachere Darstellung erreicht man unter Verwendung der
Drehimpulsmatrizen in kartesischer Form
\begin{gather}
  \label{eq:3.8}
  (\bm L_i)_{jk} = -\ii \varepsilon_{ijk} \\
  \notag
  \bm L_1 =
  \begin{pmatrix}
    \\ && -\ii\\ & \ii\\
  \end{pmatrix}
  \;,\quad
  \bm L_2 =
  \begin{pmatrix}
    &&\ii \\ \\ -\ii\\
  \end{pmatrix}
  \;,\quad
  \bm L_3 =
  \begin{pmatrix}
    &-\ii& \\ \ii\\ \\
  \end{pmatrix}
  \;.
\end{gather}
Sie erfüllen die Kommutatorrelation
\begin{equation}
  \label{eq:3.9}\relax
  [\bm L_i,\bm L_j] = \ii \varepsilon_{ijk} \bm L_k \;.
\end{equation}
Bildet man den Vektor
\begin{equation}
  \label{eq:3.10}
  \bm L = (\bm L_1, \bm L_2, \bm L_3)^\tp
\end{equation}
so findet man
\begin{subequations}
  \begin{equation}
    \label{eq:3.11a}
    -\ii \hat n \bm L = -\ii (n_1 \bm L_1 + n_2 \bm L_2 + n_3 \bm L_3)
    \eqrel{eq:3.7}{=} \hat n \times
  \end{equation}
  und
  \begin{equation}
    \label{eq:3.11b}
    (-\ii \hat n \bm L)^2 = \hat n \otimes \hat n - \mathds 1 \;.
  \end{equation}
\end{subequations}
Damit bildet man für $\bm R$:
\begin{align}
  \notag
  \bm R &\eqrel{eq:3.7}{=} \mathds 1\cos\varphi + (\mathds 1-\cos\varphi) \hat n \otimes \hat n + \sin\varphi \hat n\times \\
  \notag
  &\eqrel{eq:3.11a}[eq:3.11b]{=} \mathds 1\cos\varphi + (\mathds 1-\cos\varphi) [\mathds 1 + (-\ii\hat n\bm L)^2] - \ii\sin\varphi \hat n\bm L \\
  \notag
  &= \mathds 1 + (-\ii\hat n\bm L)^2 \underbrace{(\mathds 1-\cos\varphi)}_{2\sin^2\varphi/2} - (\ii\hat n\bm L)\sin\varphi \\
  \label{eq:3.12}
  &= \mathds 1 + 2(\ii\hat n\bm L)^2 \sin^2\frac{\varphi}{2} - (\ii\hat n\bm L)\sin\varphi = \ee^{-\ii\hat n\bm L\varphi} \;.
\end{align}
Das Rechnen mit den Rotationsmatrizen kann mühselig sein.
\begin{example}
  Wie lautet die neue Drehachse der Verkettung $\bm R(\hat m,\chi) \bm
  R(\hat n,\varphi)$?
\end{example}
Abhilfe schafft hier die Ausnutzung der Beziehung zu $\mathrm{SU}(2)$.

\subsection{Die Gruppe \texorpdfstring{$\mathrm{SU}(2)$}{SU(2)} und ihr
  Bezug zu \texorpdfstring{$\mathrm{SO}(3)$}{SO(3)}}

\begin{equation}
  \label{eq:3.13}
  \mathrm{SU}(2) = \{ \bm A \in \mathbb C^{2\times2} \mid \bm A^\dagger\bm A = \mathds 1, \det\bm A = 1 \} \;.
\end{equation}
Die Matrizen enthalten
\begin{itemize}
\item acht reelle Parameter
\item vier reelle Bedingungen aus $\bm A^\dagger\bm A = \mathds 1$
\item eine reelle Bedingung aus $\det\bm A = 1$
\end{itemize}
was drei unabhängige Parameter ergibt.  Eine sehr elegante
Schreibweise ist
\begin{subequations}
  \begin{equation}
    \label{eq:3.14a}
    \bm A =
    \begin{pmatrix}
      x_0 + \ii x_3 & x_2 + \ii x_1 \\
      -x_2 + \ii x_1 & x_0 - \ii x_3 \\
    \end{pmatrix}
  \end{equation}
  mit der Nebenbedingung
  \begin{equation}
    \label{eq:3.14b}
    x_0^2 + x_1^2 + x_2^2 + x_3^2 = 1
  \end{equation}
\end{subequations}
Im Vektorraum dieser Matrizen bilden die Pauli-Matizen
\begin{equation}
  \label{eq:3.15}
  \sigma_1 =
  \begin{pmatrix}
    0 & 1 \\
    1 & 0 \\
  \end{pmatrix}
  \;,\quad
  \sigma_2 =
  \begin{pmatrix}
    0 & -\ii \\
    \ii & 0 \\
  \end{pmatrix}
  \;,\quad
  \sigma_3 =
  \begin{pmatrix}
    1 & 0 \\
    0 & -1 \\
  \end{pmatrix}
  \;,\quad
  \mathds 1 =
  \begin{pmatrix}
    1 & 0 \\
    0 & 1 \\
  \end{pmatrix}
\end{equation}
eine Basis, sodass
\begin{equation}
  \label{eq:3.16}
  \bm A \eqrel{eq:3.14a}[eq:3.15]{=} x_0 \mathds 1 + \ii x_1 \sigma_1 + \ii x_2 \sigma_2 + \ii x_3 \sigma_3 \;.
\end{equation}
\begin{subequations}
  Mit
  \begin{equation}
    \label{eq:3.17a}
    \sigma_i \sigma_j = \delta_{ij} \mathds 1 + \ii \varepsilon _{ijk} \sigma_k
  \end{equation}
  und
  \begin{equation}
    \label{eq:3.17b}
    \bm s = \frac12 \bm\sigma = \frac12 (\sigma_1,\sigma_2,\sigma_3)^\tp
  \end{equation}
\end{subequations}
findet man
\begin{equation}
  \label{eq:3.18}
  \bm A = \ee^{-\ii\varphi\hat n\bm s} = \mathds 1 \cos\frac\varphi2 - \ii \hat n\bm\sigma \sin\frac\varphi2 \;.
\end{equation}
Aus einem Vergleich mit \eqref{eq:3.14a} liest man ab:
\begin{equation}
  \label{eq:3.19}
  x =
  \begin{pmatrix}
    x_0 \\ x_1 \\ x_2 \\ x_3 \\
  \end{pmatrix}
  =
  \begin{pmatrix}
    \cos\varphi/2 \\
    \hat n \sin\varphi/2 \\
  \end{pmatrix}
  \;,\quad
  0 \leq \varphi \leq 4\pi \;.
\end{equation}
Gleichung \eqref{eq:3.19} zeigt, dass es eine bijektive Abbildung
\begin{align*}
  \mathrm{SU}(2) &\leftrightarrow S^3 = \text{3-Sphäre} \\
  \ee^{-\ii\varphi\hat n\bm s} &\leftrightarrow
  \begin{pmatrix}
    \cos\varphi/2 \\
    \hat n \sin\varphi/2 \\
  \end{pmatrix}
\end{align*}
gibt.  Darüber hinaus gilt analog zu \eqref{eq:3.9} die Relation
\begin{equation}
  \label{eq:3.20}\relax
  [\bm s_i,\bm s_j] = \ii \varepsilon_{ijk} \bm s_k
\end{equation}
und somit gibt es einen Homomorphismus
\begin{subequations}
  \begin{align}
    \notag
    \phi\colon \mathrm{SU}(2) &\to \mathrm{SO}(3) \\
    \label{eq:3.21a}
    \ee^{-\ii\varphi\hat n\bm s} &\mapsto \ee^{-\ii\varphi\hat n\bm L} \\
    \label{eq:3.21b}
    \mathds 1 \cos\frac\varphi2 - \ii \hat n\bm\sigma \sin\frac\varphi2
    &\eqrel{eq:3.21a}[eq:3.18,eq:3.7]{\mapsto}
    \mathds 1\cos\varphi + (\mathds 1-\cos\varphi) \hat n \otimes \hat n + \sin\varphi \hat n\times
  \end{align}
\end{subequations}
Dabei beachte man aber $\bm A(\hat n,\varphi)$ und $\bm A(\hat
n,\varphi+2\pi)$ werden auf dasselbe Element in $\mathrm{SO}(3)$
abgebildet und entsprechen
\begin{equation*}
  x =
  \begin{pmatrix}
    \cos\varphi/2 \\
    -\hat n \sin\varphi/2 \\
  \end{pmatrix}
  \;,\quad
  \tilde x =
  \begin{pmatrix}
    \cos(\varphi/2+\pi) \\
    -\hat n \sin(\varphi/2+\pi) \\
  \end{pmatrix}
  = - x
\end{equation*}
d.h.\ ein Punkt und sein Antipode auf $S^3$.
\begin{equation*}
  \ker\phi = \{ \mathds 1,-\mathds 1 \} \cong \mathbb Z_2 \;.
\end{equation*}
Aus dem Homomorphiesatz~\ref{thm:2.12} folgt
\begin{equation}
  \label{eq:3.22}
  \underbrace{\mathrm{SO}(3)}_{\im\phi} \cong \underbrace{\mathrm{SU}(2)/\mathbb Z}_{\ker\phi} \cong S^3/\mathbb Z = P^3
\end{equation}
wobei $P^3$ der projektive Raum und damit die Menge aller Strahlen in
$\mathbb R^4$ ist.

\begin{figure}[tb]
  \centering
  \missingfigure{}
  \caption{Veranschaulichung des projektiven Raumes $P^2$.}
  \label{fig:7.1}
\end{figure}

Achtung, es gilt
\begin{equation*}
  \mathrm{SO}(3) \cong \mathrm{SU}(2)/\mathbb Z_2 \cong \mathrm{O}(3)/\mathbb Z_2 \;.
\end{equation*}
Daraus folgt nicht $\mathrm{SU}(2) \cong \mathrm{O}(3)$!


%%% Local Variables:
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "de_DE"
%%% TeX-master: "../Gruppentheorie.tex"
%%% End:
