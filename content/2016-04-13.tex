%Vorlesung 13-04-2016 Marcel

Irreduzible Darstellung der $\mathrm{SO}(3)$: $D^\ell$ mit Grad
$2l+1$.  Im kubischen Kristall sind die Unterräume zu $\ell$ nicht
mehr invariant, oder anders ausgedrückt, die Darstellungen $D^\ell$
sind, wenn sie auf die Symmetrieelemente eingeschränkt werden, nicht
mehr irreduzibel. Eine Zerlegung nach den irreduziblen Darstellungen
von $\mathcal O$ nach~\eqref{eq:4.35} ist erforderlich.
\begin{equation}
  \label{eq:5.18}
  n_\alpha^{(\ell)} = \frac{1}{\lvert\mathcal O\rvert} \sum_{g \in \mathcal O} \chi^{\ast (\ell) }(g) \chi^{(\ell)}(g) \;,\quad \alpha \in A_1, A_2, E, F_1, F_2  
\end{equation}
Man findet:
\begin{subequations}
  \begin{alignat}{3}
    D^{(0)} &= A_1 && \textrm{bleibt invariant} 
    \label{eq:5.19a}\\
    D^{(1)} &= F_1 && \textrm{bleibt invariant}
    \label{eq:5.19b} \\
    D^{(2)} &= E \oplus F_2 
    \label{eq:5.19c} \\
    D^{(3)} &= A_2 \oplus F_1 \oplus F_2 
    \label{eq:5.19d} \\
    D^{(4)} &= A_1 \oplus E \oplus F_1 \oplus F_2
    \label{eq:5.19e} \\
    D^{(5)} &= E \oplus 2 F_1 \oplus F_2 
    \label{eq:5.19f} 
  \end{alignat}
\end{subequations}
Man kann also Folgendes aus der Symmetire ablesen: Die $S$- und
$P$-Zustände bleiben entartet, sie bilden auch im kubischen Kristall
einen invarianten Unterraum.  Die fünf $d$-Zustände zerfallen in einen
zweidimensionalen und einen dreidimensionalen Unterraum, d.h.~es gibt
eine Termaufspaltung.

\begin{figure}[tb]
  \centering
  \begin{tikzpicture}[gfx]
    \foreach \i in {1,...,5}
    \draw (-3+.5*\i,0) -- ++(.3,0);
    \foreach \i in {1,...,2}
    \draw (.5*\i,.5) -- ++(.3,0);
    \foreach \i in {1,...,3}
    \draw (.5*\i,-.5) -- ++(.3,0);
    \draw[MidnightBlue,->] (-.2,0) -- (.5,.5);
    \draw[MidnightBlue,->] (-.2,0) -- (.5,-.5);
  \end{tikzpicture}
  \caption{Termaufspaltung der fünf $d$-Zustände.}
  \label{fig:19-1}
\end{figure}

\section{Symmetriereduktion des Eigenwertproblems}

\subsection{Lösen durch Diagonalisieren einer Matrixdarstellung}

Wähle eine beliebige orthonormal Basis: $\ket{e_j}$.  Dann gilt
\begin{align*}
  H\ket{\psi} &= E \ket{\psi} \\
  \sum_j \braket{e_k|H|e_j} \braket{e_j|\psi} &= E \braket{e_k|\psi}
\end{align*}
also
\begin{equation}
  \label{eq:5.20}
  \sum_j H_{kj} \psi_j = E \psi_k
\end{equation}
Gleichung~\eqref{eq:5.20} ist ein Eigenwertproblem, welches aus der
linearen Algebra bekannt ist.

Frage: Kann man das Eigenwertproblem~\eqref{eq:5.20} vereinfachen?
Gibt es eine optimale Basis?

\subsubsection{Symmetrieangepasste Basis}

Das Ziel ist es, die Matrix $H_{kj}$ aus~\eqref{eq:5.20} in
Blockdiagonalform zu bringen.
\begin{equation*}
  \begin{tikzpicture}[
    baseline=-\the\dimexpr\fontdimen22\textfont2\relax,
    every left delimiter/.style={xshift=1ex},
    every right delimiter/.style={xshift=-1ex}]
    \matrix[matrix of math nodes,left delimiter=(,right delimiter=)] (m)
    {
      |[draw,inner sep=.3em]| * \\
      & |[draw,inner sep=.6em]| * \\
      && |[draw,inner sep=.9em]| * \\
      &&& |[inner xsep=0pt,rotate=-45]| \dots \\
    };
    \path[font=\Large] (m.south west) --
      node[pos=.25] {$0$} node[pos=.75] {$0$} (m.north east);
  \end{tikzpicture}
\end{equation*}
Dazu zerlegt man den Hilbertraum $V$ in invariante Teilräume
\begin{equation}
  \label{eq:5.21}
  V = \bigoplus_{\alpha=1}^k \bigoplus_{i=1}^{n_\alpha} V_i^{(\alpha)}
\end{equation}
mit entsprechender othonormal Basis nach Abschnitt 4.7.3, nämlich
\begin{equation}
  \label{eq:5.22}
  \left\{ \ket{e^{(\alpha)}_{ij}} \, , \alpha=1, \ldots k \land i =1, \ldots n_\alpha \land j =1, \ldots d_\alpha \right\}
\end{equation}
Behauptung: Wenn die $\{ \ket{e^{(\alpha)}_{ij}} \}$ Basis zur irreduziblen Darstellung $\alpha$ bilden, so tun das auch die Vektoren $\{ H \ket{e^{(\alpha)}_{ij}} \}$.

\begin{proof}
  \begin{subequations}
    \begin{align}
      \label{eq:5.23a}
      D(g) \ket{e^{(\alpha)}_{ij}} &\eqrel{eq:5.2}{=} \sum_{l=1}^{d_\alpha} \ket{e^{(\alpha)}_{ij}} D^{(\alpha)}_{ij}(g) \\
      \label{eq:5.23b}
      D(g) H|\ket{e^{(\alpha)}_{ij}} &\eqrel{eq:5.3}{=} H D(g) \ket{e^{(\alpha)}_{ij}} \eqrel{eq:5.23a}{=}  \sum_{l=1}^{d_\alpha} H \ket{e^{(\alpha)}_{ij}} D^{(\alpha)}_{ij}(g)
    \end{align}
  \end{subequations}
  Die Vektoren $H \ket{e^{(\alpha)}_{ij}}$ transformieren sich nach der
  Darstellung $D^{(\alpha)}$, sind also ganz sicher
  \begin{equation*}
    H \ket{e^{(\alpha)}_{ij}} \in V^{(\alpha)} = \sum_{i=1}^{(\alpha)} V_i^{(\alpha)}
  \end{equation*}
\end{proof}
Eine wichtige Folgerung daraus ist, dass die Matrixdarstellung von $H$
in Blöcke zerfällt.
\begin{equation}
  \label{eq:5.25}
  H =
  \begin{pmatrix}
    H^{11} &&&&& \phantom{H^{kk}}\rlap{\quad$\} V^{1}$} \\
    & H^{22} &&&& \phantom{H^{kk}}\rlap{\quad$\} V^{2}$} \\
    && \ddots \\
    &&& H^{\alpha\alpha} && \phantom{H^{kk}}\rlap{\quad$\} V^{\alpha}$} \\
    &&&& \ddots \\
    &&&&& H^{kk} \rlap{\quad$\} V^{k}$} \\
  \end{pmatrix}
\end{equation}
Dabei gilt
\begin{equation}
  \label{eq:5.26}
  \braket{e^{(\alpha)}_{ij}|H|e^{(\beta)}_{kl}} = 0 \quad\text{für}\quad \alpha \neq \beta
\end{equation}
Die Blöcke $H^{\alpha \alpha}$ lassen sich wieder vereinfachen und
noch einmal in Blöcke zerlegen, denn es gilt

\begin{theorem}
  \label{thm:5.3}
  Seien $\{ v^{\alpha}_m \in V, m=1,\dots,d_\alpha \}$ und $\{ w^\beta_n
  \in V, n=1,\dots,d_\beta \}$ Basisvektoren zu den irreduziblen
  unitären Darstellungen $\alpha$ und $\beta$ von $\mathcal G$, so gilt
  \begin{equation*}
    \braket{v^\alpha_m|v^\beta_n} = \delta_{\alpha,\beta} \delta_{m,n} C^{(\alpha)}
  \end{equation*}
  wobei $C^{(\alpha)}$ nicht von der Zeile der Darstellung $m=n$ abhängt.
\end{theorem}

\begin{proof}
  \begin{align}
    \notag
    \braket{v^\alpha_m|w^\beta_n} &= \frac{1}{\lvert\mathcal G\rvert}
    \sum_{g \in \mathcal G} \, \braket{D(g) v^\alpha_m| D(g) w^\beta_n} \\
    \notag
    &= \frac{1}{\lvert\mathcal G\rvert} \sum_{g \in \mathcal G} \sum_{r,s}
    D^{*(\alpha)}_{rm}(g) D^{(\beta)}_{sn}(g) \braket{v^\alpha_m|w^\beta_n} \\
    \label{eq:5.27}
    &\eqrel{eq:4.21}{=} \delta_{\alpha \beta} \delta_{mn} \underbrace{
      \frac{1}{d_\alpha} \sum_r \braket{v^\alpha_r|w^\beta_r}
    }_{=C^{(\alpha)}}
  \end{align}
  wobei $D(g)$ alle Darstellungen enthält. Der Koeffizient
  $C^{(\alpha)}$ heißt reduziertes Matrixelement.
\end{proof}

Folgerung für das Eigenwertproblem: Setze
\begin{align*}
  v_m^{(\alpha)} &= \ket{e^{(\alpha)}_{im}} \\
  w_n^{(\beta)} &= H \ket{e^{(\beta)}_{jn}} 
\end{align*}
so folgt mit~\eqref{eq:5.27}
\begin{equation}
  \label{eq:5.28}
  \braket{e^{(\beta)}_{jn}|e^{(\beta)}_{jn}}
  = \delta_{\alpha,\beta} \delta_{m,n}  C^{(\alpha)}_{ij}
\end{equation}
Das Matrixelement hängt nicht von Zählindex $m=n$ der Vektoren
innerhalb der Unterräume $V^{(\alpha)}_n$ ab. Folgich gilt
%MATRIX 3
mit $\ket{e^{(\alpha)}_{n_\alpha m}}$ jeweils Zeilen aus allen
Vielfachheiten. Als Konsequenz ergibt sich:
\begin{itemize}
\item $H_{n_\alpha m}$ jeweils Zeilen aus allen Vielfachheiten. Als Konsequenz ergibt sich:
\item $H^{\alpha,\alpha}$ ist aus $d_\alpha$ identischen Blöcken aufgebaut.
\item Man erhält $n_\alpha$ (in der Regel verschiedene) Eigenwerte $E^{(\alpha)}_i$
\item Diese sind $d_\alpha$-fach entartet (identische Matrizen)
\item Die Symmetrie allein unterteilt den Unterraum $V^{(\alpha)}$
  nicht weiter.  Wie die Zerlegung~\eqref{eq:5.23a} aussieht, hängt
  vom Hamiltonoperator $H$ ab.
\end{itemize}
Ein Beispiel: Im freien Raum sind die $h$-Zustände, also Quantenzahl
$l=5$
\begin{equation*}
  D^{(5)} \eqrel{eq:5.19b}{=} E \oplus 2 F_1 \oplus F_2 
\end{equation*}
entartet im kubischen Kristall lautet die Zerlegung des Unterraums:
%MATRIX4
Damit existieren vier verschiedene Eigenwerte der Matrix $H^{(5)}$.

%%% Local Variables:
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "deutsch"
%%% TeX-master: "../Gruppentheorie.tex"
%%% End:
