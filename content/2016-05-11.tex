Mit dem Weg $\{ \bm\alpha(t), t \in \mathbb R, \bm\alpha(0) = 0 \}$ im
Parameterraum gilt
\begin{equation}
  \label{eq:6.16}
  \left.\frac{\diff g}{\diff t}\right|_{t=0}
  = \sum_\kappa \left.\frac{\partial g}{\partial \alpha^\kappa} \right|_{\bm\alpha=0} \left.\frac{\diff \alpha^\kappa}{\diff t}\right|_{t=0}
  = \sum_\kappa \left.\frac{\diff \alpha^\kappa}{\diff t}\right|_{t=0} J_\kappa
\end{equation}
Ist Vektor im Tangentialraum.

\subsection{Linkstransport eines Tangentialvektors}

Sei $h \in \mathcal G$ und $g(t)$ ein Weg nach \eqref{eq:6.16}, also
durch die Identität mit Tangentialvektor
\begin{equation}
  \label{eq:6.17}
  \bm v = \left.\frac{\diff g}{\diff t}\right|_0 \;.
\end{equation}
Dann ist $hg(t)$ ein Weg durch $h$ mit Tangentialvektor
\begin{equation}
  \frac{\diff}{\diff t} h g(t)
\end{equation}
aus dem Tangentialraum $T_h \mathcal G$ an $h$.

\begin{notice}[Bezeichnung:]
  $\frac{\diff}{\diff t} h g(t)$ ist der \acct{linkstransportierte
    Vektor} $L_h \bm v \in T_h \mathcal G$.
\end{notice}

Die Menge der linkstransportierten Vektorfelder
\begin{align*}
  \mathcal G &\to T \mathcal G \\
  g &\mapsto \bm v(g) = L_g \bm v \in T_g \mathcal G, \bm v \in T_e \mathcal G
\end{align*}
ist isomorph zur Liealgebra.

\subsection{Kommutator und Liealgebra}

Bilden die Tangentialvektoren eine Algebra?  Dazu muss im
Tangentialraum ein Produkt existieren.  Eine Liealgebra liegt vor,
wenn als binäre Operation (Produkt) der Kommutator vorliegt:
\begin{equation}
  \label{eq:6.18}
  \begin{aligned}
    [\cdot,\cdot] \colon v \times v &\to v \\
    (A,B) &\to [A,B]
  \end{aligned}
\end{equation}
Für die Basisvektoren muss also $[J_\kappa,J_\lambda]$ wieder eine
Linearkombination der $J_\rho$ sein
\begin{equation}
  \label{eq:6.19}\relax
  [J_\kappa,J_\lambda] = C^\rho_{\kappa\lambda} J_\rho
\end{equation}
Die $C^\rho_{\kappa\lambda}$ heißen \acct{Strukturkonstanten}.  Dabei
ist die Liealgebra immer durch die Gruppenstruktur gegeben.  Die
Strukturkonstanten bestimmen sich aus den Strukturfunktionen
$\bm\varphi(\bm\alpha,\bm\beta)$.  Um das zu sehen starten wir mit
\begin{equation}
  \label{eq:6.20}
  g(\bm\alpha) = g(\bm\beta) g(\bm\alpha_0) \quad\text{für ein festes $\bm\alpha_0$}
\end{equation}
Es folgt
\begin{subequations}
  \begin{align}
    \label{eq:6.21a}
    \bm\alpha &= \bm\varphi(\bm\beta,\bm\alpha_0) \\
    \label{eq:6.21b}
    \bm\beta &= \bm\varphi(\bm\alpha,\bar{\bm\alpha}_0)
  \end{align}
\end{subequations}
\begin{subequations}
  \begin{align}
    \label{eq:6.22a}
    \left. \frac{\partial g(\bm\alpha)}{\partial \alpha^\kappa} \right|_{\bm\alpha_0}
    &\eqrel{eq:6.20}{=}
    \left. \frac{\partial g(\bm\beta)}{\partial \beta^\rho} \right|_{\bm\beta=\bm\varphi(\bm\alpha_0,\bar{\bm\alpha}_0)=\bm\varepsilon=0}
    \left. \frac{\partial \beta^\rho}{\partial \alpha^\kappa} \right|_{\bm\alpha=\bm\alpha_0} g(\bm\alpha_0)
    \intertext{wobei}
    \label{eq:6.22b}
    \left. \frac{\partial g(\bm\beta)}{\partial \beta^\rho} \right|_{\bm\beta=0} &= J_\rho \\
    \label{eq:6.22c}
    \left. \frac{\partial \beta^\rho}{\partial \alpha^\kappa} \right|_{\bm\alpha=\bm\alpha_0} &= \left. \frac{\partial \varphi^\rho}{\partial \alpha^\kappa} \right|_{\bm\alpha_0} = \lambda^\rho_\kappa (\bm\alpha_0)
  \end{align}
\end{subequations}
\begin{subequations}
  \begin{align}
    \label{eq:6.23a}
    \left. \frac{\partial g(\bm\alpha)}{\partial \alpha^\kappa} \right|_{\bm\alpha_0} &= \lambda^\rho_\kappa(\bm\alpha_0) J_\rho g(\bm\alpha_0)
    \intertext{insbesondere $\bm\alpha_0 = \bm\alpha$}
    \label{eq:6.23b}
    \left. \frac{\partial g(\bm\alpha)}{\partial \alpha^\kappa} \right|_{\bm\alpha} = \lambda^\rho_\kappa(\alpha) J_\rho g(\bm\alpha)
  \end{align}
\end{subequations}
$\bm\alpha_0$ ist weiterhin fest aber auf den Wert $\alpha$ gesetzt,
an dem die Ableitung gebildet wird.

Partielle Differentiation
\begin{align}
  \frac{\partial^2 g}{\partial \alpha^\sigma \partial \alpha^\kappa}
  \notag
  &= \frac{\partial \lambda^\rho_\kappa}{\partial \alpha^\sigma} J_\rho g(\bm\alpha) + \lambda^\rho_\kappa(\bm\alpha) J_\rho \frac{\partial g(\bm\alpha)}{\partial \alpha^\sigma} \\
  \label{eq:6.24}
  &= \frac{\partial \lambda^\rho_\kappa}{\partial \alpha^\sigma} J_\rho g(\bm\alpha) + \lambda^\rho_\kappa(\bm\alpha) J_\rho \lambda^\tau_\sigma J_\tau g(\bm\alpha)
\end{align}
Speziell für $\bm\alpha = \bm\varepsilon = 0$:
\begin{subequations}
  \label{eq:6.25}
  \begin{align}
    \label{eq:6.25a}
    \lambda^\rho_\kappa(\bm\varepsilon) &\eqrel{eq:6.22c}{=} \left. \frac{\partial \varphi^\rho(\bm\alpha,\bm\varepsilon)}{\partial \alpha^k} \right|_{\bm\alpha=\bm\varepsilon} = \frac{\partial \alpha^\rho}{\partial \alpha^\kappa} = \delta^\rho_\kappa \\
    \label{eq:6.25b}
    g(\bm\varepsilon) &= 0
  \end{align}
\end{subequations}
Also insbesondere
\begin{equation}
  \label{eq:6.26}
  \left. \frac{\partial^2 g}{\partial \alpha^\sigma \partial \alpha^\kappa} \right|_{\bm\alpha=\bm\varepsilon}
  \eqrel{eq:6.24}[eq:6.25]{=} \left( J_\kappa J_\sigma + \frac{\partial \lambda^\rho_\kappa}{\partial \alpha^\sigma}(\varepsilon) J_\rho \right) e
\end{equation}
Da die zweiten Ableitungen vertauschen:
\begin{gather}
  \notag
  \left.\frac{\partial^2 g}{\partial \alpha^\sigma \partial \alpha^\kappa}\right|_{\bm\alpha=\bm\varepsilon}
  - \left.\frac{\partial^2 g}{\partial \alpha^\kappa \partial \alpha^\sigma}\right|_{\bm\alpha=\bm\varepsilon}
  = 0 \\
  \label{eq:6.27}
  \eqrel{eq:6.26}{=} [J_\kappa,J_\sigma] - \underbrace{\left\{ \frac{\partial \lambda^\rho_\sigma}{\partial \alpha^\kappa} - \frac{\partial \lambda^\rho_\kappa}{\partial \alpha^\sigma} \right\}}_{C^\rho_{\kappa\sigma}} J_\rho
\end{gather}
Die Strukturkonstanten lassen sich eindeutig aus den
Strukturfunktionen und damit aus der Gruppenstruktur bestimmen.

\begin{notice}[Anmerkung:]
  Das Produkt der Liealgebra \eqref{eq:6.18} (der Kommutator) ist
  nicht assoziativ.  Es gilt die Jacobi-Identität
  \begin{equation}
    \label{eq:eq:6.28}\relax
    [J_\kappa,[J_\sigma,J_\rho]] + [J_\sigma,[J_\rho,J_\kappa]] + [J_\rho,[J_\kappa,J_\sigma]] = 0
  \end{equation}
\end{notice}

\subsection{Der umgekehrte Weg: Von der Liealgebra zur Liegruppe}

Wir haben gesehen, dass die Liealgebra eindeutig aus der Liegruppe
folgt.  Gilt das auch umgekehrt?

\begin{theorem} (Satz von Cartan)
  \label{thm:6.1}
  Sei $L$ eine $\nu$-dimensionale reelle Liealgebra.  Dann gibt es bis
  auf Isomorphismen genau eine einfach zusammenhängende
  $\nu$-dimensionale Liegruppe $\mathcal G$, deren Liealgebra $L$ ist.
\end{theorem}

\subsubsection{Bemerkungen}

Einfach zusammenhängend heißt: Jeder geschlossene Weg kann stetig zu
einem Punkt kontrahiert werden.  Viele Liegruppen besitzen dieselbe
Liealgebra (z.B.\ $\mathrm{U}(1)$ und $\mathbb R$ oder
$\mathrm{SU}(2)$ und $\mathrm{SO}(3)$).  Nur eine ist einfach
zusammenhängend.  Sie heißt universelle Überlgerungsgruppe.  Alle
anderen Liegruppen mit derselben Liealgebra werden durch die
Faktorisierung mit einem diskreten Normalteiler $H$ gewonnen, z.B.
\begin{equation*}
  \mathrm{SO}(3) = \mathrm{SU}(2)/\mathbb Z_2 \;.
\end{equation*}

\subsubsection{Exponentialabbildung}

Den Weg von der Liealgebra zur Liegruppe stellt die
Exponentialabbildung her.  Dazu betrachtet man das Element
\begin{equation}
  \label{eq:6.29}
  M = \alpha^\mu J_\mu
\end{equation}
der Liealgebra.  Dann bildet
\begin{equation}
  \label{eq:6.30}
  \ee^{t M} = \sum_{n=0}^\infty \frac{1}{n!} t^n M^n = 1 + t M + \frac{1}{2} t^2 M^2 + \cdots
\end{equation}
eine eindimensionale Untergruppe der Liegruppe.  Jedes Element der
Liegruppe ist in einer solchen Untergruppe oder ist das Produkt aus
Elementen dieser.

\subsubsection{Beispiel}

$\mathrm{SO}(3)$: $(\bm L_i)_{jk} = -\ii\varepsilon_{ijk}$ bilden die
Liealgebra.  Die Elemente der Liegruppe sind
\begin{equation}
  \label{eq:6.31}
  \ee^{\ii (\omega_1 \bm L_1 + \omega_2 \bm L_2 + \omega_3 \bm L_3)}
\end{equation}

\section{Die Liegruppen $\mathrm{SU}(1)$}

Die Liegruppen $\mathrm{SU}(n)$ haben in der Physik eine herausragende
Bedeutung und sollen hier genauer untersucht werden.  Sie sind
halbeinfach und für solche Gruppen gibt es einen systematischen Weg
die Basiszustände der irreduziblen Darstellungen zu finden.

Es gibt für $g \in \mathrm{SU}(n)$
\begin{subequations}
  \begin{align}
    \label{eq:6.32a}
    g^\dagger g &= 1 \\
    \label{eq:6.32b}
    \det g &= 1
  \end{align}
\end{subequations}
Der Parameterbereich ist offensichtlich beschränkt, denn für die
Matrixelemente gilt
\begin{equation}
  \label{eq:6.33}
  \sum_i \lvert a_{ij} \rvert^2 \eqrel{eq:6.32a}{=} 1
\end{equation}
sind kompakt.

\subsection{Cartan-Weyl-Basis der Liealgebra $\mathrm{SU}(n)$}

Unabhängige Parameter:
\begin{itemize}
\item Die Matrizen haben $2 n^2$ reelle Elemente
\item aus \eqref{eq:6.32a} folgen $n^2$ Bedingungen
\item aus \eqref{eq:6.32b} folgt eine Bedingung
\end{itemize}
Es bleiben also $n^2 - 1$ unabhängige Parameter.  Die Dimension des
Tangentialraums (der Liealgebra) $\dim V = n^2 - 1$.



%%% Local Variables:
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "deutsch"
%%% TeX-master: "../Gruppentheorie.tex"
%%% End:
