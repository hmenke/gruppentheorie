\chapter{Liegruppen}

Liegruppen sind kontinuierliche Gruppen, die auch Eigenschaften einer
differenzierbaren Mannigfaltigkeit besitzen.  Diese zusätzlichen
Eigenschaften lassen sich zum Verständnis und in der praktischen
Anwendung nutzen.

\section{Was sind Liegruppen?}

\subsection{Vergleich mit diskreten Gruppen}

\subsubsection{Indizes in diskreten Gruppen}

In endlichen Gruppen lassen sich die Elemente einfach mit Zahlen aus
$\mathbb Z$ durchnummerieren.

\begin{example}
  $C_n = \braket{c_n}$.  Eine mögliche Nummerierung ist
  \begin{equation}
    \label{eq:6.1}
    J_i = c_n^i \;,\quad i=0,1,\dots,n-1
  \end{equation}
  mit der Indexmenge $I = \{ 0,1,\dots,n-1 \} \subset \mathbb Z$.
\end{example}

Prinzipiell lässt sich dann die Verknüpfung der Gruppenelemente allein
durch die Indizes ausdrücken, indem man eine Funktion $\varphi$
einführt
\begin{equation}
  \label{eq:6.2}
  \varphi \colon I \times I \;,\quad
  i,j \mapsto \varphi(i,j) \text{ mit } g_{\varphi(i,j)} = g_i \cdot g_j
\end{equation}

\begin{example}
  $C_n$: $\varphi(i,j) = i+j \bmod n$.
\end{example}

Eine häufige Notation ist $i \to \bar\imath$ für das inverse Element
und $\varepsilon$ für das neutrale Element.  Das funktioniert auch für
abzählbar unendliche Gruppen, $I = \mathbb Z$ oder $\mathbb Z^n$.

\subsubsection{kontinuierliche Gruppen}

Hängen von einem oder mehreren kontinuierlichen Indizes ab
\begin{equation}
  \label{eq:6.3}
  \{ g(\bm\alpha) , \bm\alpha=(\alpha_1,\dots,\alpha_n) \in \mathbb R^n \}
\end{equation}

\begin{example}
  Matrixgruppen:
  \begin{equation}
    \label{eq:6.4}
    g =
    \begin{pmatrix}
      \alpha_{11} & \cdots & \alpha_{1n} \\
      \vdots & \ddots & \vdots \\
      \alpha_{n1} & \cdots & \alpha_{nn} \\
    \end{pmatrix}
    \in \mathrm{GL}(n,\mathbb R)
  \end{equation}
\end{example}

Die Anzahl der unabhängigen reellen Parameter nennt man
\acct{Dimension der Gruppe}.  Für die Parameter gilt dann z.B.
\begin{equation}
  \label{eq:6.5}
  \begin{pmatrix}
    \alpha_1 & \alpha_2 \\
    \alpha_3 & \alpha_4 \\
  \end{pmatrix}
  \begin{pmatrix}
    \beta_1 & \beta_2 \\
    \beta_3 & \beta_4 \\
  \end{pmatrix}
  =
  \begin{pmatrix}
    \alpha_1 \beta_1 + \alpha_2 \beta_3 & \alpha_1 \beta_2 + \alpha_2 \beta_4 \\
    \alpha_3 \beta_1 + \alpha_4 \beta_3 & \alpha_3 \beta_2 + \alpha_4 \beta_4 \\
  \end{pmatrix}
  =
  \begin{pmatrix}
    \gamma_1 & \gamma_2 \\
    \gamma_3 & \gamma_4 \\
  \end{pmatrix}
\end{equation}
Die Gruppeneigenschaft überträgt sich auf den Parameterraum und es
gilt für die Funktionen $\varphi$:
\begin{equation}
  \label{eq:6.6}
  \bm\gamma = \bm\varphi(\bm\alpha,\bm\beta) \;.
\end{equation}

\subsection{Algebraische topologische und differenzierbare Eigenschaften}

\subsubsection{Definition der Liegruppe}

\begin{definition}
  \label{def:6.1}
  Eine Liegruppe der Dimension $\nu$ ist eine Menge $\mathcal G$, die
  zugleich Gruppe und zusammenhängende $\nu$-dimensionale
  Mannigfaltigkeit ist und in der beide Eigenschaften analytisch
  vereint sind.
\end{definition}

\begin{enumerate}
\item Ist $G(\bm\alpha) g(\bm\beta) = g(\bm\gamma)$, so gilt
  \begin{equation}
    \label{eq:6.7}
    \bm\gamma = \bm\varphi(\bm\alpha,\bm\beta) \;,\quad
    \gamma^\mu = \varphi^\mu(\alpha^1,\alpha^2,\dots,\alpha^\nu,\beta^1,\beta^2,\dots,\beta^\nu)
  \end{equation}
  und die \acct{Strukturfunktion} $\varphi^\mu$ sind analytisch.
\item Die Gruppeneigenschaften spiegeln sich in den Eigenschaften
  der Strukturfunktionen $\varphi$ wider.
  \begin{itemize}
  \item Assoziativität
    \begin{equation}
      \label{eq:6.8}
      \bm\varphi(\bm\varphi(\bm\alpha,\bm\beta),\bm\gamma)
      = \bm\varphi(\bm\alpha,\bm\varphi(\bm\beta,\bm\gamma))
    \end{equation}
  \item Sei $g(\varepsilon) = e$ das Einselement (meist $\bm\varepsilon = \bm 0$), so gilt
    \begin{equation}
      \label{eq:6.9}
      \varphi(\bm\varepsilon,\bm\alpha) = \bm\varphi(\bm\alpha,\bm\varepsilon) = \bm\alpha
    \end{equation}
  \item Ist $g(\bar{\bm\alpha}) = g(\bm\alpha^{-1})$, so gilt
    \begin{equation*}
      \bm\varphi(\bar{\bm\alpha},\bm\alpha) = \bm\varphi(\bm\alpha,\bar{\bm\alpha}) = \bm\varepsilon
    \end{equation*}
    und $\bar{\bm\alpha}$ hängt analytisch von $\bm\alpha$ ab.
  \end{itemize}
\end{enumerate}

\subsubsection{Mannigfaltigkeiten}

Differenzierbare Mannigfaltigkeiten sind Verallgemeinerungen von
glatten Flächen im Raum.  Eine differenzierbare Mannigfaltigkeit $M^n$
der Dimension $n$ ist ein (hausdorffscher topologischer) Raum, für den
ein Koordinatennetz oder ein System von überlappenden
Koordinatennetzen (einen Atlas) gibt.

Kartenabbildung
\begin{equation*}
  \psi\colon U \to \mathbb R^n \;,\quad p \mapsto \psi(p)
\end{equation*}
Mit einem Atlas überdeckt man die gesamte Mannigfaltigkeit $M =
\bigcup_\alpha U_\alpha$.  In einer nichtleeren Schnittmenge gilt: Bei
einem differenzierbaren Atlas sind alle $\psi_{\alpha\beta}$
differenzierbar.

\subsubsection{Beispiele}

$\mathrm{GL}(1,\mathbb R) = \mathbb R \setminus \{ 0 \}$
\begin{itemize}
\item $\varphi(\alpha) = \alpha$
\item Der positive Halbstrahl $\mathrm{GL}_+(1,\mathbb R)$ ist eine
  Liegruppe.  Er enthält das neutrale Element $\varepsilon=1$ und das
  zu $\alpha$ inverse Element $\bar\alpha = 1/\alpha$.
\end{itemize}

$\mathrm U(1) = \{ \ee^{\ii\phi} \mid 0 \leq \phi < 2 \pi \} \cong S^1 \cong \mathrm{SO}(2)$
\begin{itemize}
\item Ist eindimensionale differenzierbare Mannigfaltigkeit
\item $\varphi(\ee^{\ii\phi}) = \phi \bmod 2 \pi$
\item neutrales Element $\phi = 0$
\item inverses Element $\bar\phi = 2 \pi - \phi$
\end{itemize}

\subsubsection{weitere Anwendungen}

\begin{definition}
  \label{def:6.2}
  Eine Liegruppe heißt
  \begin{itemize}
  \item kompakt, wenn der Parameterbereich beschränkt ist (vgl.~$\mathrm U(1)$)
  \item einfach, wenn sie nur die trivialen Normalteiler $\mathcal G$ und $\{ e \}$ enthält
  \item halbeinfach, wenn außer $\mathcal G$ alle anderen Normalteiler abelsch sind (vgl.~Def.~\ref{def:2.20})
  \end{itemize}
\end{definition}

Mit dem Wissen über Mannigfaltigkeiten können Liegruppen abstrakter
als in Def.~\ref{def:6.1} eingeführt werden.

\begin{definition}
  \label{def:6.3}
  Eine Liegruppe ist zugleich Gruppe und differenzierbare
  Mannigfaltigkeit $\mathcal G$, wobei das Gruppenprodukt
  \begin{equation*}
    \mathcal G \times \mathcal G \to \mathcal G
  \end{equation*}
  und die Abbildung von $g$ nach $g^{-1}$ differenzierbare Abbildungen
  sind.
\end{definition}

\section{Liegruppe und Liealgebra}

Die Liealgebra ist auf dem Tangentialraum der Mannigfaltigkeit
gegeben.

\begin{example}
  zweidimensionale Liegruppe mit Koeffizienten $\alpha^1,\alpha^2 \in
  \mathbb R$.
\end{example}

\subsection{Liealgebra einer Matrixgruppe}

Die Gruppe habe die Elemente $\mathcal G = \{ g(\bm\alpha) \}$.  Die
Liealgebra ist der Tangentialraum am Einselement (normalerweise
$\bm\alpha = \bm\varepsilon = 0$).  Sie wird von den Erzeugenden
\begin{equation}
  \label{eq:6.11}
  J_k = \left. \frac{\partial g}{\partial \alpha^k} \right|_{\bm\alpha=0}
\end{equation}
aufgepsannt.

\begin{example}
  $\mathrm U(1) = \{ \ee^{\ii\phi} \mid 0 \leq \phi < 2 \pi \}$
  \begin{equation}
    \label{eq:6.12}
    \left. \frac{\diff}{\diff\phi} \ee^{\ii\phi} \right|_{\phi=0} = \ii
  \end{equation}
  Liealgebra von $\mathrm U(1)$ ist $\ii\mathbb R$ oder anders
  ausgedrückt: $\ii$ ist infinitesimaler Erzeuger von $\mathrm U(1)$.
  \begin{equation}
    \label{eq:6.13}
    \ee^{\ii\phi} \approx 1 + \ii\phi
  \end{equation}
\end{example}

\begin{example}
  $\mathrm{SO}(3)$
  \begin{subequations}
    \begin{align}
      \label{eq:6.14a}
      g(\omega^1,0,0) &\eqrel{eq:3.7}{=}
      \mathds 1 \cos\omega_1 + \hat e_1 \otimes \hat e_1 (1-\cos\omega_1) + \sin\omega_1 \hat e_1 \times \\
      \label{eq:6.14b}
      \left. \frac{\diff g}{\diff \omega^1} \right|_{\omega^1 = 0} &\eqrel{eq:6.14a}{=} \hat e_1 \times =
      \begin{pmatrix}
        0 & 0 & 0 \\
        0 & 0 & 1 \\
        0 & -1 & 0 \\
      \end{pmatrix}
      \eqrel{eq:3.8}{=} -\ii \bm L_1 \quad\text{mit}\quad (\bm L_i)_{jk} = -\ii\varepsilon_{ijk}
    \end{align}
  \end{subequations}
\end{example}

%%% Local Variables:
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "deutsch"
%%% TeX-master: "../Gruppentheorie.tex"
%%% End:
