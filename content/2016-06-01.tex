Ein Gewichtsvektor heißt positiv, wenn die erste nichtverschwindende
Komponente positiv ist.

Der Vektor $\bm\omega_a$ heißt größer als $\bm\omega_b$, wenn
$\bm\omega_a-\bm\omega_b$ positiv ist.

Es gibt ein maximales positives Gewicht (ergibt sich aus ähnlichen
Argumenten wie $\lvert m\rvert \leq l$ beim Drehimpuls) dieses
definiert äquivalent zu den Casimir-Operatoren eindeutig die
Darstellung.

\subsection{\texorpdfstring{$\mathrm{SU}(2)$}{SU(2)}}

Hier ist $n=2$.  Es gibt also $n-1$ Operatoren in der
Cartan-Unteralgebra
\begin{equation*}
  H_1 \eqrel{eq:6.38a}{=} \frac{1}{2} \sigma_z
\end{equation*}
und $n-1$ Casimir-Operatoren
\begin{align*}
  C_2 &\eqrel{eq:6.42}{=}
  \sum_{i_1,j_1,i_2,j_2=1}^3 \varepsilon_{i_1,j_1,j_2} \varepsilon_{i_2,j_2,j_1} J_{i_1} J_{i_2} \\
  &= - \sum_{i_1,j_1,i_2,j_2=1}^3 \varepsilon_{i_1,j_1,j_2} \varepsilon_{i_2,j_1,j_2} J_{i_1} J_{i_2} \\
  &= \sum_{i_1,i_2=1}^3 \delta_{i_1,i_2} J_{i_1} J_{i_2} \\
  &= \sum_{i_1=1}^3 J_{i_1}^2 = J_x^2 + J_y^2 + J_z^2 = \bm J^2
\end{align*}
Bekanntes Ergebnis aus der Quantenmechanik.  Es gibt gemeinsame
Eigenzustände zu $\bm J^2$ und $J_z$.  Der Gewichtsvektor enthält den
Eigenwert zu $H_1=J_z= \sigma_z/2$, er ist eindimensional, damit ist
das auch der einzige linear unabhängige Wurzelvektor.

Man berechnet
\begin{align}
  \notag
  H_1 E_{12} \ket{Jm} &\eqrel{eq:6.38a}{=} \frac{1}{2} \sigma_z \sigma_+ \ket{Jm} \\
  \notag
  &= \left( \sigma_+ \frac{1}{2} \sigma_z + \sigma_+ \right) \ket{Jm} \\
  \notag
  &= (E_{12} H_1 + E_{12}) \ket{Jm} = (m+1) E_{12} \ket{Jm} \\
  \label{eq:6.49}
  &\eqrel{eq:6.36a}{=} (m+r_{1,12}) \ket{Jm} \;,\quad r_{1,12} = 1 = -r_{1,21}
\end{align}
Damit ist die Form des eindimensionalen Wurzelgitters gegeben.
\begin{center}
  \begin{tikzpicture}
    \draw[->] (-4,0) -- (4,0);
    \node[dot,label={below:\strut$\cdots$}] at (-3,0) {};
    \node[dot,label={below:\strut$m-2$}] at (-2,0) {};
    \node[dot,label={below:\strut$m-1$}] at (-1,0) {};
    \node[dot,label={below:\strut$m  $}] at ( 0,0) {};
    \node[dot,label={below:\strut$m+1$}] at ( 1,0) {};
    \node[dot,label={below:\strut$m+2$}] at ( 2,0) {};
    \node[dot,label={below:\strut$\cdots$}] at (3,0) {};
  \end{tikzpicture}
\end{center}

\minisec{Welche Werte sind möglich?}

Weitere Bedingungen entstehen durch Forderungen an die
Zustandsvektoren $\ket{Jm}$.  Hier die Normierung des Vektors $E_{12}
\ket{Jm}$.
\begin{align*}
  \braket{Jm|E_{12}^+ E_{12}|Jm}
  &= \braket{Jm|J_- J_+|Jm} = \braket{Jm|J^2-J_z^2-J_z|Jm} \\
  &= (J(J+1)-m^2-m) \braket{Jm|Jm} \geq 0
\end{align*}
es folgt
\begin{subequations}
  \begin{align}
    \label{eq:6.50a}
    J(J+1) - m(m+1) &\geq 0 \\
    \label{eq:6.50b}
    J(J+1) - m(m-1) &\geq 0
  \end{align}
\end{subequations}
und damit für die extremalen Werte von $m$
\begin{subequations}
  \begin{align}
    \label{eq:6.51a}
    \text{aus \eqref{eq:6.50a}} \quad m > 0 \quad J(J+1) &\geq m(m+1) \to m_{\max} = J \\
    \notag
    \text{aus \eqref{eq:6.50b}} \quad m < 0 \quad J(J+1) &\geq m(m-1) = -\lvert m\rvert(-\lvert m\rvert-1) = \lvert m\rvert(\lvert m\rvert+1) = J \\
    \label{eq:6.51b}
    & \to m_{\min} = -J
  \end{align}
\end{subequations}
Da wegen \eqref{eq:6.49} $-J=m_{\min} = m_{\max} - h = J-k$ mit $k\in\mathbb N$ gelten muss, folgt $h=2J$ oder $J=h/2$, also
\begin{equation}
  \label{eq:6.52}
  J\in\{0,1/2,1,3/2,\ldots\}
\end{equation}
Wurzelgitter sind eindeutig gegeben:
\begin{enumerate}
\item zur Darstellung $J=0$
  \begin{tikzpicture}[baseline=(X.base)]
    \draw[->] (-1.5,0) node (X) {\strut} -- (1.5,0);
    \draw (0,2pt) -- (0,-2pt);
    \node[dot,label={below:\strut$0$}] at (0,0) {};
  \end{tikzpicture}
\item zur Darstellung $J=1/2$
  \begin{tikzpicture}[baseline=(X.base)]
    \draw[->] (-1.5,0) node (X) {\strut} -- (1.5,0);
    \draw (0,2pt) -- (0,-2pt);
    \node[dot,label={below:\strut$-1/2$}] at (-0.5,0) {};
    \node[dot,label={below:\strut$ 1/2$}] at ( 0.5,0) {};
  \end{tikzpicture}
\item zur Darstellung $J=1$
  \begin{tikzpicture}[baseline=(X.base)]
    \draw[->] (-1.5,0) node (X) {\strut} -- (1.5,0);
    \draw (0,2pt) -- (0,-2pt);
    \node[dot,label={below:\strut$-1$}] at (-1,0) {};
    \node[dot,label={below:\strut$ 1$}] at ( 1,0) {};
  \end{tikzpicture}
\end{enumerate}
Entartungsindex wird nicht benötigt, da alle Zustände eindeutig durch
$\ket{Jm}$ gegeben sind.

\subsection{\texorpdfstring{$\mathrm{SU}(3)$}{SU(3)}}

\subsubsection{Fundamentaldarstellung}

Hier ist $n=3$.  Es gibt also $n-1=2$ Operatoren in der
Cartan-Unteralgebra, zwei Casimir-Operatoren, zwei linear unabhängige
Wurzelvektoren und ein zweidimensionales Wurzelgitter.

Üblicherweise liegen uns die Gruppen $\mathrm{SU}(n)$ bereits in
Matrixform vor, siehe z.B.\ \eqref{eq:6.39a}, \eqref{eq:6.39b},
\eqref{eq:6.40a}, \eqref{eq:6.40b}.  \acct{Fundamentaldarstellung} aus
der das Wurzelsystem abgelesen werden kann.

Für $\mathrm{SU}(3)$ das Gewichtssystem aus \eqref{eq:6.39a},
\eqref{eq:6.39b}
\begin{subequations}
  \begin{align}
    \label{eq:6.51a}
    \bm\omega^{(1)} &= \frac{1}{2}
    \begin{pmatrix}
      1 \\ 1/\sqrt{3} \\
    \end{pmatrix} \\
    \label{eq:6.51b}
    \bm\omega^{(2)} &= \frac{1}{2}
    \begin{pmatrix}
      -1 \\ 1/\sqrt{3} \\
    \end{pmatrix} \\
    \label{eq:6.51c}
    \bm\omega^{(3)} &=
    \begin{pmatrix}
      0 \\ - 1/\sqrt{3}
    \end{pmatrix}
  \end{align}
\end{subequations}
Wurzelvektoren
\begin{subequations}
  \begin{align}
    \label{eq:6.52a}
    r_{12} &= -r_{21} = \bm\omega^{(1)} - \bm\omega^{(2)} \eqrel{eq:6.51a}[eq:6.51b]{=}
    \begin{pmatrix}
      1 \\ 0 \\
    \end{pmatrix} \\
    \label{eq:6.52b}
    r_{23} &= -r_{32} = \bm\omega^{(2)} - \bm\omega^{(3)} = \frac{1}{2}
    \begin{pmatrix}
      -1 \\ \sqrt{3} \\
    \end{pmatrix} \\
    \label{eq:6.52c}
    r_{31} &= -r_{13} = \bm\omega^{(3)} - \bm\omega^{(1)} = -\frac{1}{2}
    \begin{pmatrix}
      1 \\ \sqrt{3} \\
    \end{pmatrix}
  \end{align}
\end{subequations}
Nach \eqref{eq:6.43} unabhängig von der gewählten Darstellung.  Hat
man einen Gewichtsvektor, so folgen daraus alle anderen über zwei
Vektoren aus \eqref{eq:6.52a}, \eqref{eq:6.52b}, \eqref{eq:6.52c}.

\begin{figure}[tb]
  \centering
  \missingfigure{}
  \caption{Graphische Darstellung in der Fundamentaldarstellung.}
  \label{fig:21-1}
\end{figure}

\subsubsection{Eigenspektrum}

Eine Darstellung wird üblicherweise durch die Casimir-Operatoren
angegeben.  In $\mathrm{SU}(3)$ keine sinnvolle physikalische
Bedeutung.  Man wählt zwei andere Quantenzahlen.

Innerhalb der Darstellung: $H_1$ und $H_2$ charakterisieren die
Zustände, aber nicht eindeutig.  Entartungsindex $\mu$ wird benötigt.

Zur Bezeichnung der Zustände: Operatoren mit physikalischer Anwendung.
\begin{subequations}
  \begin{align}
    \label{eq:6.53a}
    I_\pm &= E_{12}^R \pm \ii E_{12}^I \\
    \label{eq:6.53b}
    I_3 &= H_1
  \end{align}
\end{subequations}
Komponenten des Isospin.
\begin{subequations}
  \begin{align}
    \label{eq:6.54a}
    U_\pm &= E_{23}^R \pm \ii E_{23}^I \\
    \label{eq:6.54b}
    V_\pm &= E_{13}^R \pm \ii E_{13}^I \\
    \label{eq:6.54c}
    Y &= \frac{2}{\sqrt{3}} H_2
  \end{align}
\end{subequations}
Zusätzlich sind für Anwendung noch interessant die abhängigen
Operatoren
\begin{align*}
  U_3 &= \frac{3}{4} Y - \frac{1}{2} I_3 \\
  V_3 &= \frac{3}{4} Y + \frac{1}{2} I_3
\end{align*}
Alle Zustände einer Darstellung (eines Multiplets) erhält man dann in
folgender Form:

Der Ausgangszustand ist der Gewichtsvektor mit maximalem positiven
Gewicht ($\bm\omega_1 = i_3 = {}$Eigenwert von $I_3$ ist maximal).  Das ist
\begin{equation}
  \label{eq:6.55}
  \bm\omega_{\max} = \frac{1}{2}
  \begin{pmatrix}
    n_1 \\ \frac{1}{\sqrt{3}} (n_1 + 2 n_2) \\
  \end{pmatrix}
\end{equation}
Dieser Zustand erfüllt $V_+ \ket{\omega_{\max}} = U_+
\ket{\omega_{\max}} = 0$.  Alle weiteren folgen durch Anwendung von
Kombinationen von $I_\pm$, $V_\pm$, $U_\pm$, denn dies sind alles Auf-
und Absteigeoperatoren wie die Kommutatorrelationen
\begin{equation}
  \label{eq:6.56}\relax
  [x_3,x_\pm] = \pm x_\pm \;,\quad x \in \{ I,V,U \}
\end{equation}
zeigen.  Vgl.\ $[J_z,J_\pm] = \pm J_\pm$ für $\mathrm{SU}(2)$.
Gleichung~\eqref{eq:6.56} zeigt aber auch $\mathrm{SU}(2)$ und damit
die Eigenschaften des Spektrums von $\mathrm{SU}(2)$ treten mehrfach
on $\mathrm{SU}(3)$ auf.

\begin{figure}[tb]
  \centering
  \missingfigure{}
  \caption{Wurzelsystem von $\mathrm{SU}(3)$.}
  \label{fig:21-2}
\end{figure}

%%% Local Variables:
%%% mode: latex
%%% mode: flyspell
%%% ispell-local-dictionary: "deutsch"
%%% TeX-master: "../Gruppentheorie.tex"
%%% End:
